var str = 'hello world';
var num = 1;
var booleanVar = false;
var arr = ['a', 'b', 'c'];
var obj = {
  name: 'jack',
  lastname: 'baur',
  age: 44,
};

var i;

for (i = 0; i < arr.length; i++) {
  console.log('value is ', arr[i]);
}

for (i of arr) {
  console.log('i is ', i);
}

for (i = 0; i < str.length; i++) {
  console.log('value is ', str[i]);
}

for (i of str) {
  console.log('i is ', i);
}

// objects, we have to use in
for (i in obj) {
  console.log('key is ', i, ', value is ', obj[i]);
}

// if you want to use key and value, use "in" for objects, arrays and strings

// if you want to use only value in case of arrays and strings, use "of"

for (i in arr) {
  console.log('key is ', i, ', value is ', arr[i]);
}

for (i in str) {
  console.log('key is ', i, ', value is ', str[i]);
}

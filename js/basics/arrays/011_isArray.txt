// isArray
var arr = [1, 2, 3];
var check = Array.isArray(arr);
check;
true


console.log(Array.isArray([1, 2, 3]));
true

console.log(Array.isArray('xyz'));
false

console.log(Array.isArray({key: 1}));
false

console.log(Array.isArray(false));
false

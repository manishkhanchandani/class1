// promise.all with reject, it will go to catch block if any one of the promise is rejected

var p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('one');
    resolve('one');
  }, 15000);
});
var p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('two');
    reject('two');
  }, 5000);
});
var p3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('three');
    resolve('three');
  }, 10000);
});
// run all three in parallel
Promise.all([p1, p2, p3])
  .then((message) => {
    console.log('message is ', message);
  })
  .catch((err) => {
    console.log('err is ', err);
  });

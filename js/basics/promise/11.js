function f() {
  var p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject('p1');
    }, 3000);
  });
  var p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject('p2');
    }, 2000);
  });
  var p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('p3');
    }, 1000);
  });

  Promise.any([p1, p2, p3])
    .then((data) => {
      console.log('data is ', data);
    })
    .catch((err) => {
      console.log('err is ', err);
    })
    .finally(() => {
      console.log(
        'we are here irrespective of the resolve or reject condition'
      );
    });
}
f();

// Promise
// I promise to do something
// Result is either promise is completed / resolved, or it is rejected or failed, or could not keep the promise

var a = 2;
var p = new Promise((resolve, reject) => {
  if (a === 2) {
    resolve('success');
  } else {
    reject('failed');
  }
});
p.then((message) => {
  console.log('message is ', message);
}).catch((err) => {
  console.log('err is ', err);
});

// basic syntax
// when some data will appear or will show up in future,

// 3 states:
// PENDING STATE
// FULFILLED STATE
// REJECTED STATE

var p1 = new Promise((resolve, reject) => {
  // you call the server to get data
  // or call setTimeout so that some code should run after certain seconds.
  setTimeout(() => {
    resolve('hello world');
    reject('hello world');
  }, 5000);
});

p1.then((data) => {
  console.log('data is ', data);
}).catch((err) => {
  console.log('err is ', err);
});

/*
 Promise is created by variable = new Promise();
 We have arrow function inside Promise.
 This arrow function gets 2 paramaters - one is called resolve and another is called reject
 Whenever we get good or success response from server, we pass that data back using resolve.
 But if we get bad data from server, we pass that data back using reject.

 Once promise is created, we can use it by calling .then();
 .then has arrow function inside it, with one parameter and that is the data which was retured from resolve part.

 if we have bad data then the code will not go inside .then().

 it will go inside the catch block.
 Catch block is also having the arrow function, and it will get error as a permeter and this error is nothing but data returned from reject block.


*/

var p1 = new Promise((resolve, reject) => {
  // you call the server to get data
  // or call setTimeout so that some code should run after certain seconds.
  setTimeout(() => {
    reject('hello world');
  }, 5000);
});

p1.then(
  (data) => {
    console.log('data is ', data);
  },
  (err) => {
    console.log('err is ', err);
  }
);

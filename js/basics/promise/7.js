var p = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(10);
  }, 5000);
});

p.then((value) => {
  console.log('value is ', value);
  return value + 10;
})
  .then((value) => {
    console.log('value is ', value);
    return value + 10;
  })
  .then((value) => {
    console.log('value is ', value);
    return value + 10;
  })
  .then((value) => {
    console.log('value is ', value);
    return value + 10;
  })
  .then((value) => {
    console.log('value is ', value);
    return value + 10;
  })
  .catch((err) => {
    console.log('err is ', err);
  });

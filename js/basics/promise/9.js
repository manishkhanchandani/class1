function f1() {
  var p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('p1 10s');
      resolve('p1');
    }, 10000);
  });
  var p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('p2 15s');
      resolve('p2');
    }, 15000);
  });
  var p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('p3 10s');
      reject('p3');
    }, 5000);
  });

  Promise.all([p1, p2, p3])
    .then((data) => {
      console.log('data is ', data);
    })
    .catch((err) => {
      console.log('err is ', err);
    });
}
f();

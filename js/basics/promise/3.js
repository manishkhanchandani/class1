var count = 0;

function testPromise(ms) {
  var funcCount = ++count;
  console.log(funcCount, ' Start sync code');
  var p = new Promise((resolve, reject) => {
    console.log(funcCount, ' Promise Started, Async Code started: ', ms);
    setTimeout(() => {
      resolve(funcCount);
    }, ms);
  });

  p.then((val) => {
    console.log(val, ', Promise fulfilled, async code ended.');
  }).catch((err) => {
    console.log(err, ' Promise rejecetd, async code ended.');
  });

  console.log(funcCount, ' End sync code');
}

testPromise(10000);
testPromise(15000);
testPromise(5000);

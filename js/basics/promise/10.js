function f() {
  var p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('p1');
    }, 15000);
  });
  var p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('p2');
    }, 5000);
  });
  var p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject('p3');
    }, 10000);
  });

  Promise.allSettled([p1, p2, p3])
    .then((data) => {
      console.log('data is ', data);
    })
    .catch((err) => {
      console.log('err is ', err);
    });
}
f();

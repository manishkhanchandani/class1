// Promise.all
// so if one is completed early then other two, then other two will wait for the first one to finish.
// but if you want that anyone who is done should resolve it, then use race instead of all, and then it will resolve only the first one.

var p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('one');
    resolve('one');
  }, 15000);
});
var p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('two');
    resolve('two');
  }, 5000);
});
var p3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('three');
    resolve('three');
  }, 10000);
});
// run all three in parallel
Promise.all([p1, p2, p3])
  .then((message) => {
    console.log('message is ', message);
  })
  .catch((err) => {
    console.log('err is ', err);
  });

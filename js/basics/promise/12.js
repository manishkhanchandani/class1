function f1() {
  var p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('hello');
      resolve('hello');
    }, 2000);
  });

  p1.then((data) => {
    console.log('data is ', data);
  }).catch((err) => {
    console.log('err is ', err);
  });
}

f1();

async function f2() {
  var p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('hello');
      resolve('hello');
    }, 2000);
  });

  var data = await p1;

  console.log('data is ', data);
}
f2();

async function f3() {
  var isloading = true;
  try {
    var p1 = new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log('hello');
        reject('hello');
      }, 2000);
    });

    var data = await p1;

    console.log('data is ', data);
  } catch (err) {
    console.log('err is ', err);
  } finally {
    isloading = false;
    console.log('done');
  }
}
f3();

async function f() {
  try {
    var p1 = new Promise((resolve, reject) => {
      setTimeout(() => {
        reject('p1');
      }, 3000);
    });
    var p2 = new Promise((resolve, reject) => {
      setTimeout(() => {
        reject('p2');
      }, 2000);
    });
    var p3 = new Promise((resolve, reject) => {
      setTimeout(() => {
        reject('p3');
      }, 1000);
    });

    var res = await Promise.any([p1, p2, p3]);
    console.log('res is ', res);
  } catch (err) {
    console.log('err is ', err);
  }
}
f();

import React from 'react';

const questions = [
  {
    id: 'abc123',
    question: 'Who is the president of United States of America?',
    options: ['Trump', 'Biden', 'Amitabh Bachhan', 'Indira Gandhi'],
    correct: 1,
  },
  {
    id: 'xyz456',
    question: 'Who is the prime minister of India?',
    options: ['Rahul Gandhi', 'Yogi', 'Modi', 'Myself'],
    correct: 2,
  },
  {
    id: 'mno888',
    question: 'Who will win the t20 world cup?',
    options: ['Pakistan', 'England', 'Australia', 'South Africa'],
    correct: 0,
  },
];

function App() {
  const [step, setStep] = React.useState(0);
  const [formValues, setFormValues] = React.useState({});
  const [total, setTotal] = React.useState(0);
  const [show, setShow] = React.useState(false);

  const showResult = () => {
    let count = 0;
    questions.forEach((rec) => {
      const usersAnswer = formValues[rec.id];
      const correctAnswer = rec.correct;
      if (usersAnswer === correctAnswer) {
        count++;
      }
    });

    setTotal(count);
    setShow(true);
  };
  return (
    <div>
      <h1>Quiz</h1>
      <div>
        <div>
          <div>
            Question {step + 1}: {questions[step].question}
          </div>
          <div>
            <h3>Options Are:</h3>
            <ul>
              {questions[step].options.map((option, index) => {
                return (
                  <li key={questions[step].id + index}>
                    <input
                      type='radio'
                      name={questions[step].id}
                      value={index}
                      onChange={(e) => {
                        setFormValues((fV) => {
                          return {
                            ...fV,
                            [e.target.name]: parseInt(e.target.value, 10),
                          };
                        });
                      }}
                    />{' '}
                    {option}
                  </li>
                );
              })}
            </ul>
          </div>
          <div>
            {step > 0 && (
              <button
                onClick={() => {
                  setStep(step - 1);
                }}
              >
                Back
              </button>
            )}
            {step < questions.length - 1 && (
              <button
                onClick={() => {
                  setStep(step + 1);
                }}
              >
                Next
              </button>
            )}
            {step === questions.length - 1 && (
              <button onClick={showResult}>Finish</button>
            )}
          </div>
        </div>
        {show && (
          <div>
            <h3>Result</h3>
            <div>Total Number of Correct Answers: {total}</div>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;

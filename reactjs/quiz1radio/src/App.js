import React from 'react';

const questions = [
  {
    id: 'abc1',
    question: 'who is the presedient of the USA',
    options: ['Trump', 'Biden', 'Ashok'],
    correct: 1,
  },
  {
    id: 'xyz2',
    question: 'who is the prime minister of india',
    options: ['Rahul', 'Yogi', 'Modi'],
    correct: 0,
  },
  {
    id: 'mno8',
    question: 'what is the world largest continent',
    options: ['Asia', 'America', 'Russia'],
    correct: 1,
  },
];

function App() {
  const [step, setStep] = React.useState(0);
  const [formValues, setFormValues] = React.useState({});
  const [total, setTotal] = React.useState(0);
  const [show, setShow] = React.useState(false);

  const showResults = () => {
    let count = 0;
    questions.forEach((rec) => {
      const userAns = formValues[rec.id];
      const correctAns = rec.correct;
      if (userAns === correctAns) {
        count++;
      }
    });
    setTotal(count);
    setShow(true);
  };
  return (
    <div>
      <div>QUIZ</div>
      <div>
        <div>
          Question {step + 1}: {questions[step].question}
        </div>
        <div>
          <h3>Options Are: </h3>
          <ul>
            {questions[step].options.map((option, index) => {
              return (
                <li key={questions[step].id + index}>
                  <input
                    type='radio'
                    name={questions[step].id}
                    value={index}
                    onChange={(e) => {
                      setFormValues((fv) => {
                        return {
                          ...fv,
                          [e.target.name]: parseInt(e.target.value, 10),
                        };
                      });
                    }}
                  />
                  {option}
                </li>
              );
            })}
          </ul>
        </div>
        <div>
          {step > 0 && (
            <button
              onClick={() => {
                setStep(step - 1);
              }}
            >
              {' '}
              Back
            </button>
          )}

          {step < questions.length - 1 && (
            <button
              onClick={() => {
                setStep(step + 1);
              }}
            >
              {' '}
              Next
            </button>
          )}
        </div>
        <div>
          {step === questions.length - 1 && (
            <button onClick={showResults}> Finish</button>
          )}
        </div>
        {show && (
          <div>
            <h3> Result </h3>
            <div>Total number of correct answers: {total}</div>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;

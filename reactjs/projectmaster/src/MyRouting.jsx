import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from './components/Login';
import Register from './components/Register';
import Logout from './components/Logout';
import Forgot from './components/Forgot';
import Change from './components/Change';
import Master from './components/Master';
import Master2 from './components/Master/Master';
import Detail from './components/Detail';
import Create from './components/Create';
import Delete from './components/Delete';
import Playground from './components/Playground';
import Home from './components/Home';
import Add from './components/Add';
import Edit from './components/Edit';
import Example1 from './components/Example1';
import Todo from './components/Todo';
import Test3 from './components/Test3';

function MyRouting() {
  return (
    <div id='myrouting'>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/example1' component={Example1} />
          <Route exact path='/todo' component={Todo} />
          <Route exact path='/playground' component={Playground} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/logout' component={Logout} />
          <Route exact path='/forgot' component={Forgot} />
          <Route exact path='/change' component={Change} />
          <Route exact path='/master' component={Master} />
          <Route exact path='/master2' component={Master2} />
          <Route exact path='/detail/:id' component={Detail} />
          <Route exact path='/add' component={Add} />
          <Route exact path='/create' component={Create} />
          <Route exact path='/test3' component={Test3} />
          <Route exact path='/edit/:id' component={Edit} />
          <Route exact path='/delete/:id' component={Delete} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default MyRouting;

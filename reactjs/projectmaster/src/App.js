import React from 'react';
import MyRouting from './MyRouting';

function App() {
  return <MyRouting />;
}

export default App;

import React from 'react';
import Parse from 'parse';

function useGetRecords({ keyword, page, max }) {
  console.log('keyword: ', keyword);
  console.log('page: ', page);
  console.log('max: ', max);
  const [state, setState] = React.useState({
    results: null,
    count: 0,
  });
  React.useEffect(() => {
    console.log('inside keyword: ', keyword);
    const getRecords = () => {
      const Test1 = Parse.Object.extend('Test1');
      const query = new Parse.Query(Test1);
      query.matches('title', keyword, 'i');
      query.withCount();
      query
        .find()
        .then((data) => {
          setState((st) => {
            return { ...st, ...data };
          });
        })
        .catch((err) => {
          console.log('err is ', err);
        });
    };
    getRecords();
  }, [keyword]);

  console.log('state is ', state);
  return state;
}

export default useGetRecords;

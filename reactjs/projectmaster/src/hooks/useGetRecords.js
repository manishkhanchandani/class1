import React from 'react';
import Parse from 'parse';

function useGetRecords() {
  const [state, setState] = React.useState({
    results: null,
    count: 0,
  });
  React.useEffect(() => {
    const getRecords = () => {
      const query = new Parse.Query('Test1');
      query.withCount();
      query
        .find()
        .then((data) => {
          setState((st) => {
            return { ...st, ...data };
          });
        })
        .catch((err) => {
          console.log('err is ', err);
        });
    };
    getRecords();
  }, []);

  console.log('state is ', state);
  return state;
}

export default useGetRecords;

export const configToDo = [
  {
    widget: 'textfield',
    name: 'todo',
    label: 'To Do',
    type: 'text',
    valueType: 'string',
  },
];

export const configAds = [
  {
    widget: 'textfield',
    name: 'title',
    label: 'Title',
    type: 'text',
    valueType: 'string',
  },
  {
    widget: 'textarea',
    name: 'description',
    label: 'Description',
    valueType: 'string',
  },
]; // Title (textfield), Description (textarea)

import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Parse from 'parse';

function Delete(props) {
  const id = props.match.params.id;
  const [state, setState] = React.useState({
    title: '',
    description: '',
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    console.log('deleting....');
    const Post = Parse.Object.extend('Test1');
    const post = new Post();
    post.id = id;

    post
      .destroy()
      .then(() => {
        console.log('deleted successfully....');
        props.history.push('/master');
      })
      .catch((err) => {
        console.log('err is ', err.message);
        console.log('error....');
      });
  };

  console.log('state is ', state);

  React.useEffect(() => {
    const Test1 = Parse.Object.extend('Test1');
    const query = new Parse.Query(Test1);
    query
      .get(id)
      .then((data) => {
        setState((st) => {
          return {
            ...st,
            title: data.get('title'),
            description: data.get('description'),
          };
        });
      })
      .catch((err) => {
        console.log('err is ', err);
      });
  }, [id]);

  return (
    <>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box sx={{ height: '100vh' }}>
          <h3>Delete Record</h3>
          <form onSubmit={handleSubmit}>
            <div>{state.title}</div>
            <div style={{ marginTop: '25px' }}>{state.description}</div>
            <div style={{ marginTop: '25px' }}>
              <Button fullWidth variant='contained' type='submit'>
                Delete This Record
              </Button>
            </div>
          </form>
        </Box>
      </Container>
    </>
  );
}

export default Delete;

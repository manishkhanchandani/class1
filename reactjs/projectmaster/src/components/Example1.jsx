import React from 'react';
// useRef in React
function Example1() {
  const [name, setName] = React.useState('');
  const [age, setAge] = React.useState('');
  const genderRef = React.useRef(null);
  const myRef = React.useRef(null);
  console.log('name is ', name);
  console.log('age is ', age);
  console.log('genderRef is ', genderRef);
  console.log('myRef is ', myRef);

  const handleMyRef = () => {
    if (!myRef.current) {
      myRef.current = 0;
    }

    myRef.current++;
  };

  const handleSubmit = () => {
    console.log('your final name is ', name);
    console.log('your final age is ', age);
    console.log('your final gender is ', genderRef.current.value);
    console.log('your final myRef is ', myRef.current);
    console.log('gender: ', genderRef);
    genderRef.current.focus();
  };
  return (
    <div>
      <h1>Example</h1>
      <div>
        <input
          type='text'
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        />
      </div>
      <div>Your name is {name}</div>
      <div>
        <input
          type='text'
          value={age}
          onChange={(e) => {
            setAge(e.target.value);
          }}
        />
      </div>
      <div>Your age is {age}</div>
      <div>
        <input type='text' ref={genderRef} id='gender' name='gender' />
      </div>
      <div>
        <button onClick={handleSubmit}>Submit</button>
      </div>
      <div>
        <button onClick={handleMyRef}>InCrease MyRef</button>
      </div>
    </div>
  );
}

export default Example1;

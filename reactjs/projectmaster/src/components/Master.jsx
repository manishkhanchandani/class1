import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Parse from 'parse';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import TablePagination from '@mui/material/TablePagination';

function Master() {
  const [state, setState] = React.useState({
    results: [],
    count: 0,
    keyword: '', // this will be changed when someone will put in the input box of keyword
  });

  const [keyword, setKeyword] = React.useState(null); // this will be changed when someone will click the search button and keyword value is updated.

  // for pagination
  const [page, setPage] = React.useState(0);
  const [max, setMax] = React.useState(10);

  const subscription = React.useRef(null);

  const handleChange = (name, value) => {
    setState((st) => {
      return { ...st, [name]: value };
    });
  };

  React.useEffect(() => {
    const create_record = (obj) => {
      setState((st) => {
        return { ...st, count: st.count + 1, results: [obj, ...st.results] };
      });
    };
    const update_record = (obj) => {
      setState((st) => {
        const res = [...st.results];
        const index = res.findIndex((rec) => {
          return obj.id === rec.id;
        });
        // update only when we find index != -1
        if (index !== -1) {
          res.splice(index, 1, obj);
        }
        return { ...st, results: res };
      });
    };
    const delete_record = (obj) => {
      setState((st) => {
        const res = [...st.results];
        const index = res.findIndex((rec) => {
          return obj.id === rec.id;
        });
        // update only when we find index != -1
        if (index !== -1) {
          res.splice(index, 1);
        }
        return { ...st, results: res, count: st.count - 1 };
      });
    };
    const getRecordsLiveQuery = async () => {
      const query = new Parse.Query('Test1');
      if (subscription && subscription.current) {
        await subscription.current.unsubscribe();
      }
      subscription.current = await query.subscribe();

      subscription.current.on('open', () => {
        console.log('subscription opened'); // this is only for information purpose that subscription is opened.
      });
      subscription.current.on('create', (object) => {
        console.log('object created', object);
        create_record(object);
      });
      subscription.current.on('update', (object) => {
        console.log('object updated', object);
        update_record(object);
      });
      subscription.current.on('enter', (object) => {
        console.log('object entered', object);
        create_record(object);
      });
      subscription.current.on('leave', (object) => {
        console.log('object left', object);
        delete_record(object);
      });
      subscription.current.on('delete', (object) => {
        console.log('object deleted', object);
        delete_record(object);
      });
      subscription.current.on('close', () => {
        console.log('subscription closed');
      });
    };
    const getRecords = () => {
      const query = new Parse.Query('Test1');
      query.descending('createdAt');
      if (keyword) {
        query.matches('title', keyword, 'i');
      }

      // page 0 (reality),  records 0, 1: page 0, starting 0, max is 2
      // page 1 (reality), records 2, 3: page 1, starting 2, max is 2
      // page 2 (reality), records 4, 5: page is 2, starting is 4, max is 2
      // page 3 (reality), records 6, 7: page is 3, starting is 6, max is 2

      query.skip(page * max);
      query.limit(max);

      query.withCount();
      query
        .find()
        .then((data) => {
          setState((st) => {
            return { ...st, ...data };
          });
        })
        .catch((err) => {
          console.log('err is ', err);
        });
    };
    getRecords();
    const process = async () => {
      await getRecordsLiveQuery();
    };
    process();
  }, [keyword, page, max]); // keyword is a dependency variable, which when changed (on clicking the search button), will call everything inside this useeffect.

  // this use effect will be called only once.
  React.useEffect(() => {
    return () => {
      console.log('returned a function in useEffect');
      if (subscription && subscription.current) {
        subscription.current.unsubscribe();
      }
    };
  }, []);

  console.log('master state is ', state);
  return (
    <>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box sx={{ height: '100vh' }}>
          <h3>Master</h3>

          <div>
            <TextField
              fullWidth
              required
              label='Keyword'
              name='keyword'
              value={state.keyword}
              onChange={(event) => {
                handleChange('keyword', event.target.value);
              }}
            />
          </div>
          <div style={{ marginTop: '15px', marginBottom: '25px' }}>
            <Button
              variant='outlined'
              fullWidth
              onClick={() => {
                setKeyword(state.keyword);
                setPage(0);
              }}
            >
              Search
            </Button>
          </div>
          <div>Total Records Found: {state.count}</div>
          <TablePagination
            component='div'
            count={state.count}
            page={page}
            onPageChange={(event, newPage) => {
              setPage(newPage);
            }}
            rowsPerPage={max}
            onRowsPerPageChange={(e) => {
              setMax(parseInt(e.target.value, 10));
              setPage(0);
            }}
            rowsPerPageOptions={[1, 2, 5, 10, 25, 50, 100]}
          />

          <div>
            {state.results &&
              state.results.length > 0 &&
              state.results.map((rec) => {
                return (
                  <Paper
                    key={rec.id}
                    elevation={3}
                    style={{ marginBottom: '15px', padding: '20px' }}
                  >
                    <a href={`/detail/${rec.id}`}>{rec.get('title')}</a>{' '}
                    <a href={`/edit/${rec.id}`}>Edit</a> |
                    <a href={`/delete/${rec.id}`}>Delete</a>
                  </Paper>
                );
              })}
          </div>
        </Box>
      </Container>
    </>
  );
}

export default Master;

import React from 'react';
import Parse from 'parse';

function Playground() {
  const register = () => {
    // to register a new user
    const user = new Parse.User();
    user.set('username', 'c@google.com');
    user.set('password', 'password');
    user.set('email', 'c@google.com');
    user.set('firstName', 'Manish');
    user.set('lastName', 'Khanchandani');
    user
      .signUp()
      .then((data) => {
        console.log('data is ', data);
      })
      .catch((err) => {
        console.log('err is ', err.message);
      });
  };

  const login = () => {
    Parse.User.logIn('c@google.com', 'password')
      .then((data) => {
        console.log('data is ', data);
      })
      .catch((err) => {
        console.log('err is ', err.message);
      });
  };
  return (
    <div>
      <h1>Playground</h1>
      <hr />
      <h3>Registration</h3>
      <button onClick={register}>Register User</button>
      <hr />
      <h3>Login</h3>
      <button onClick={login}>Login User</button>
    </div>
  );
}

export default Playground;

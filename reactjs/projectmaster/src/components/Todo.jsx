import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Parse from 'parse';
import Paper from '@mui/material/Paper';

function View() {
  const [state, setState] = React.useState({
    results: [],
    count: 0,
  });
  const subscription = React.useRef(null);
  React.useEffect(() => {
    const create_record = (obj) => {
      setState((st) => {
        return { ...st, count: st.count + 1, results: [obj, ...st.results] };
      });
    };
    const update_record = (obj) => {
      setState((st) => {
        const res = [...st.results];
        const index = res.findIndex((rec) => {
          return obj.id === rec.id;
        });
        // update only when we find index != -1
        if (index !== -1) {
          res.splice(index, 1, obj);
        }
        return { ...st, results: res };
      });
    };
    const delete_record = (obj) => {
      setState((st) => {
        const res = [...st.results];
        const index = res.findIndex((rec) => {
          return obj.id === rec.id;
        });
        // update only when we find index != -1
        if (index !== -1) {
          res.splice(index, 1);
        }
        return { ...st, results: res, count: st.count - 1 };
      });
    };
    const getRecordsLiveQuery = async () => {
      const query = new Parse.Query('Test2Todo');
      if (subscription && subscription.current) {
        await subscription.current.unsubscribe();
      }
      subscription.current = await query.subscribe();

      subscription.current.on('open', () => {
        console.log('subscription opened'); // this is only for information purpose that subscription is opened.
      });
      subscription.current.on('create', (object) => {
        console.log('object created', object);
        create_record(object);
      });
      subscription.current.on('update', (object) => {
        console.log('object updated', object);
        update_record(object);
      });
      subscription.current.on('enter', (object) => {
        console.log('object entered', object);
        create_record(object);
      });
      subscription.current.on('leave', (object) => {
        console.log('object left', object);
        delete_record(object);
      });
      subscription.current.on('delete', (object) => {
        console.log('object deleted', object);
        delete_record(object);
      });
      subscription.current.on('close', () => {
        console.log('subscription closed');
      });
    };
    const getRecords = () => {
      const query = new Parse.Query('Test2Todo');
      query.descending('createdAt');

      query.withCount();
      query
        .find()
        .then((data) => {
          setState((st) => {
            return { ...st, ...data };
          });
        })
        .catch((err) => {
          console.log('err is ', err);
        });
    };
    getRecords();
    const process = async () => {
      await getRecordsLiveQuery();
    };
    process();
  }, []);

  // this use effect will be called only once.
  React.useEffect(() => {
    return () => {
      console.log('returned a function in useEffect');
      if (subscription && subscription.current) {
        subscription.current.unsubscribe();
      }
    };
  }, []);

  console.log('master state is ', state);
  return (
    <div>
      <h3>Todo Items</h3>
      <div>Total Records Found: {state.count}</div>

      <div>
        {state.results &&
          state.results.length > 0 &&
          state.results.map((rec) => {
            return (
              <Paper
                key={rec.id}
                elevation={3}
                style={{ marginBottom: '15px', padding: '20px' }}
              >
                {rec.get('todo')}
              </Paper>
            );
          })}
      </div>
    </div>
  );
}
function Add() {
  const [state, setState] = React.useState({
    todo: '',
  });

  const handleChange = (name, value) => {
    setState((st) => {
      return { ...st, [name]: value };
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    console.log('submitting....');
    const Post = Parse.Object.extend('Test2Todo');
    const post = new Post();

    post.set('todo', state.todo);
    post.set('user', Parse.User.current());

    const acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setPublicWriteAccess(true);
    post.setACL(acl);

    post
      .save()
      .then((data) => {
        console.log('data is ', data);
        console.log('submitted successfully....');
        setState({ todo: '' });
      })
      .catch((err) => {
        console.log('err is ', err.message);
        console.log('error....');
      });
  };

  console.log('state is ', state);
  return (
    <div>
      <h3>Add Record</h3>
      <form onSubmit={handleSubmit}>
        <div>
          <TextField
            fullWidth
            required
            label='Todo Item'
            name='todo'
            value={state.todo}
            onChange={(event) => {
              handleChange('todo', event.target.value);
            }}
          />
        </div>
        <div style={{ marginTop: '25px' }}>
          <Button fullWidth variant='contained' type='submit'>
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
}
function Todo() {
  return (
    <>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box sx={{ height: '100vh' }}>
          <Add />
          <View />
        </Box>
      </Container>
    </>
  );
}

export default Todo;

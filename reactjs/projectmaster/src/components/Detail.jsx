import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Parse from 'parse';

function Detail(props) {
  const id = props.match.params.id;
  const [state, setState] = React.useState({
    title: '',
    description: '',
  });

  console.log('state is ', state);

  React.useEffect(() => {
    const Test1 = Parse.Object.extend('Test1');
    const query = new Parse.Query(Test1);
    query
      .get(id)
      .then((data) => {
        setState((st) => {
          return {
            ...st,
            title: data.get('title'),
            description: data.get('description'),
          };
        });
      })
      .catch((err) => {
        console.log('err is ', err);
      });
  }, [id]);

  return (
    <>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box sx={{ height: '100vh' }}>
          <h3>Detail Record</h3>

          <div>
            <strong>Title:</strong>
            <br />
            {state.title}
          </div>
          <div style={{ marginTop: '25px' }}>
            <strong>Description:</strong>
            <br />
            {state.description}
          </div>
        </Box>
      </Container>
    </>
  );
}

export default Detail;

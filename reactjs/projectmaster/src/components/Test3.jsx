import React from 'react';

const Child1 = React.memo((props) => {
  console.log('Child1 name is ', props.name);
  return <div>Child Name {props.name}</div>;
});
const Child2 = React.memo((props) => {
  console.log('Child2 age is ', props.age);
  return <div>Child Age {props.age}</div>;
});

function Test3() {
  const [name, setName] = React.useState('');
  const [age, setAge] = React.useState('');
  return (
    <div>
      <h1>Test 3</h1>
      <div>
        Name:{' '}
        <input
          type='text'
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        />{' '}
      </div>
      <div>
        age:{' '}
        <input
          type='text'
          value={age}
          onChange={(e) => {
            setAge(e.target.value);
          }}
        />{' '}
      </div>
      <div>
        <Child1 name={name} />
      </div>
      <div>
        <Child2 age={age} />
      </div>
    </div>
  );
}

export default Test3;

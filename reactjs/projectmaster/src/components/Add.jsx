import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Parse from 'parse';

function Add() {
  const [state, setState] = React.useState({
    title: '',
    description: '',
  });

  const handleChange = (name, value) => {
    setState((st) => {
      return { ...st, [name]: value };
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    console.log('submitting....');
    const Post = Parse.Object.extend('Test1');
    const post = new Post();

    post.set('title', state.title);
    post.set('description', state.description);
    post.set('user', Parse.User.current());

    const acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setPublicWriteAccess(true);
    post.setACL(acl);

    post
      .save()
      .then((data) => {
        console.log('data is ', data);
        console.log('submitted successfully....');
        setState({ title: '', description: '' });
      })
      .catch((err) => {
        console.log('err is ', err.message);
        console.log('error....');
      });
  };

  console.log('state is ', state);
  return (
    <>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box sx={{ height: '100vh' }}>
          <h3>Add Record</h3>
          <form onSubmit={handleSubmit}>
            <div>
              <TextField
                fullWidth
                required
                label='Title'
                name='title'
                value={state.title}
                onChange={(event) => {
                  handleChange('title', event.target.value);
                }}
              />
            </div>
            <div style={{ marginTop: '25px' }}>
              <TextField
                fullWidth
                required
                label='Description'
                name='description'
                multiline={true}
                rows={5}
                value={state.description}
                onChange={(event) => {
                  handleChange('description', event.target.value);
                }}
              />
            </div>
            <div style={{ marginTop: '25px' }}>
              <Button fullWidth variant='contained' type='submit'>
                Submit
              </Button>
            </div>
          </form>
        </Box>
      </Container>
    </>
  );
}

export default Add;

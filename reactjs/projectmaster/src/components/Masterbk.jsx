import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Parse from 'parse';

function Master() {
  const [state, setState] = React.useState({
    results: null,
    count: 0,
  });
  React.useEffect(() => {
    const getRecordsLiveQuery = async () => {
      const query = new Parse.Query('Test1');
      query.equalTo('title', 'fifth');
      const subscription = await query.subscribe();
      subscription.on('open', () => {
        console.log('subscription opened');
      });
      subscription.on('create', (object) => {
        console.log('object created', object);
      });
      subscription.on('update', (object) => {
        console.log('object updated', object);
      });
      subscription.on('enter', (object) => {
        console.log('object entered', object);
      });
      subscription.on('leave', (object) => {
        console.log('object left', object);
      });
      subscription.on('delete', (object) => {
        console.log('object deleted', object);
      });
      subscription.on('close', () => {
        console.log('subscription closed');
      });
    };
    const getRecords = () => {
      const query = new Parse.Query('Test1');
      query.equalTo('title', 'fifth');
      query.descending('createdAt');

      query.withCount();
      query
        .find()
        .then((data) => {
          setState((st) => {
            return { ...st, ...data };
          });
        })
        .catch((err) => {
          console.log('err is ', err);
        });
    };
    getRecords();
    getRecordsLiveQuery();
  }, []);

  console.log('master state is ', state);
  return (
    <>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box sx={{ height: '100vh' }}>
          <h3>Master</h3>
          <div>Total Records Found: {state.count}</div>
          <div>
            <input type='text' />
          </div>
          <div>
            {state.results &&
              state.results.length > 0 &&
              state.results.map((rec) => {
                return (
                  <Paper
                    key={rec.id}
                    elevation={3}
                    style={{ marginBottom: '15px', padding: '20px' }}
                  >
                    <a href={`/detail/${rec.id}`}>{rec.get('title')}</a>{' '}
                    <a href={`/edit/${rec.id}`}>Edit</a> |
                    <a href={`/delete/${rec.id}`}>Delete</a>
                  </Paper>
                );
              })}
          </div>
        </Box>
      </Container>
    </>
  );
}

export default Master;

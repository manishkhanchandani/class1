import React from 'react';

function DisplayCount({ count }) {
  console.log('We are in DisplayCount page');
  return <div>Total Records Found: {count}</div>;
}

export default React.memo(DisplayCount);

Master Project
Many other project

Small Projects
I will guide, you have to do it and then I will do

Facebook
Meetup.com
Twitter

    Login,
    Logout
    Registration

    Form
    	Create Record / Update Record / Delete Record

    Master - Details - Pagination

    	Title of many records in master page
    	Detail Page

STEP 1:
Install the react project
npx create-react-app projectmaster

STEP 2:
Material UI
Install Material UI
https://mui.com/getting-started/installation/

npm install @mui/material @emotion/react @emotion/styled

npm install @mui/icons-material

Put the following code in index.html

<link
  rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
/>

STEP 3:
Install the react-router-dom

npm install --save react-router-dom@5.3.0

STEP 4:
Cleanup work - Empty the contents of App.js , just keep div tag - delete current 2 import statements from App.js - empty App.css - empty index.css - Add following line in App.js
import React from 'react'; - Delete logo.svg file

STEP 5:
Create a folder components inside the src
Login.jsx  
Register.jsx  
Logout.jsx  
Forgot.jsx  
Change.jsx

        Master.jsx
        Detail.jsx
        Pagination.jsx

        Create.jsx (create and update)

        Delete.jsx

        Header.jsx

        Footer.jsx

Create sample code inside each component
import React from 'react';

function Login() {
return <div>Login</div>;
}

export default Login;

STEP 6:
Create a folder components inside the src
Login.jsx /login
Register.jsx /register
Logout.jsx /logout
Forgot.jsx /forgot
Change.jsx /change

        Master.jsx  /master
        Detail.jsx  /detail/:id
        Pagination.jsx

        Create.jsx (create and update)  /create
                                        /edit/:id

        Delete.jsx          /delete/:id

        Header.jsx

        Footer.jsx

MyRouting.jsx
Put default code

    1. import React
    2. BrowserRouter, Switch, Route - react-router-dom
    3. import all above components
    4. div, BrowserRouter, Switch, Create Route for each of above urls

import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from './components/Login';
import Register from './components/Register';
import Logout from './components/Logout';
import Forgot from './components/Forgot';
import Change from './components/Change';
import Master from './components/Master';
import Detail from './components/Detail';
import Create from './components/Create';
import Delete from './components/Delete';
function MyRouting() {
return (

<div>
<BrowserRouter>
<Switch>
<Route exact path='/login' component={Login} />
<Route exact path='/register' component={Register} />
<Route exact path='/logout' component={Logout} />
<Route exact path='/forgot' component={Forgot} />
<Route exact path='/change' component={Change} />
<Route exact path='/master' component={Master} />
<Route exact path='/detail/:id' component={Detail} />
<Route exact path='/create' component={Create} />
<Route exact path='/edit/:id' component={Create} />
<Route exact path='/delete/:id' component={Delete} />
</Switch>
</BrowserRouter>
</div>
);
}

export default MyRouting;

Call MyRouting Component in App.js

So change the App.js file as:
import React from 'react';
import MyRouting from './MyRouting';

function App() {
return <MyRouting />;
}

export default App;

STEP 7: Configuring react with parserserver

1. Install the client parse module in react
   npm install --save parse

2. open (in react) index.js and add following details:
   import Parse from 'parse';

   Parse.initialize('appId');
   Parse.serverURL = 'http://localhost:1337/parse';

https://nodejs.org/download/release/v14.17.6/node-v14.17.6-x64.msi
https://docs.parseplatform.org/js/guide/#users

STEP 8: Registration Page

1. Get the template from following page:
   https://mui.com/getting-started/templates/
   https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/sign-up/SignUp.js

2. Copy that code into the Register.jsx page

3. On Submit of the button, register the user to the parseserver.
   https://docs.parseplatform.org/js/guide/#users

4. If error comes, you have to display that error on the page.

5. If success comes, then you have to redirect user to the main page (/master)

Solution:
const [error, setError] = React.useState(null);

const register = (params) => {
// to register a new user
const user = new Parse.User();
user.set('username', params.email);
user.set('password', params.password);
user.set('email', params.email);
user.set('firstName', params.firstName);
user.set('lastName', params.lastName);
user
.signUp()
.then(() => {
props.history.push('/master');
})
.catch((err) => {
setError(err.message);
});
};
const handleSubmit = (event) => {
event.preventDefault();
const data = new FormData(event.currentTarget);

    register({
      email: data.get('email'),
      password: data.get('password'),
      firstName: data.get('firstName'),
      lastName: data.get('lastName'),
    });

};

STEP 9: Login Page

1. Get the template and replace with Login Component:
   https://mui.com/getting-started/templates/
   https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/sign-in/SignIn.js

   2. Copy the whole code from above url to Login.jsx

   3. On submit button, login the user to parseServer

   4. If error, display that error

   5. If success, redirect user to master page.

Solution:
const [error, setError] = React.useState(null);
const login = (params) => {
Parse.User.logIn(params.email, params.password)
.then(() => {
props.history.push('/master');
})
.catch((err) => {
setError(err.message);
});
};

const handleSubmit = (event) => {
event.preventDefault();
const data = new FormData(event.currentTarget);
// eslint-disable-next-line no-console

    login({
      email: data.get('email'),
      password: data.get('password'),
    });

};

Step 10: Create Home Page

1. Components folder, create a new component called as Home.jsx
2. put basic code in this component by typing rfce
3. Add a new Routing with path = / and component = Home
4. Go to registration and login page and change the path from /master to /

Create / Update Page

Master Page with Pagination
Different layout

Detail Page
Different layouts

Fun part

Step 11: Create Page

      Craigslist
         Title - input element
         Description - textarea

      Todo App
         Todo - input

      Facebook:
         News Feed

ExampleCreate.jsx
Configuration -> MyForm.jsx -> This will render the form based on configuration

1. Create Configuration
   Page:
   Name, last name, firstname, age, gender
   Page:
   Title, description, images

2. MyForm.jsx - call this form with configuration
   MyForm will render the form for you.

3. Create a state variable which will save all the information of the form in a state variable.

4. Create a state variable for errors, which will save all errors.

5. handleChange function, handleBlur Function

6. handleSubmit Function, which will first check if there are no errors, and if there are are no errors, we will submit the details of the form to the server.

7. We will redirect user to another page if everything is good.

Create a file inside src/utils/configs.js
and write following config:
export const configToDo = [
{
widget: 'textfield',
name: 'todo',
label: 'To Do',
type: 'text',
valueType: 'string',
},
];

export const configAds = [
{
widget: 'textfield',
name: 'title',
label: 'Title',
type: 'text',
valueType: 'string',
},
{
widget: 'textarea',
name: 'description',
label: 'Description',
valueType: 'string',
},
]; // Title (textfield), Description (textarea)

Create 2 state variables in Create.jsx
const [values, setValues] = React.useState();
const [errors, setErrors] = React.useState();

Also pass the props in the MyForm.jsx file
{ config, handleChange, handleBlur, values, errors }

In create.jsx, import following 2 statements:
import { configToDo } from '../utils/configs';
import MyForm from '../widgets/MyForm';

And Call myForm as follows:

<div>
   <h1>Create Record</h1>
   <MyForm config={configToDo} />
</div>

Create 2 empty functions in create.jsx

const handleChange = () => {};
const handleBlur = () => {};

And Change the MyForm attributes as follows:

<MyForm
        config={configToDo}
        values={values}
        errors={errors}
        handleChange={handleChange}
        handleBlur={handleBlur}
      />

STEP 12 : Add Page

Create a routing for new Add.jsx page with path /add

1. Create a simple form with 2 fields:
   Title
   Description

2. Creating a state variable which will hold data whenever user changes fields.

3. handleChange - I will update the state variable.

4. handleSubmit - This will be called when someone submits the form and data will be saved in the database.

Add.jsx
import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Parse from 'parse';

function Add() {
const [state, setState] = React.useState({
title: '',
description: '',
});

const handleChange = (name, value) => {
setState((st) => {
return { ...st, [name]: value };
});
};

const handleSubmit = (event) => {
event.preventDefault();

    console.log('submitting....');
    const Post = Parse.Object.extend('Test1');
    const post = new Post();

    post.set('title', state.title);
    post.set('description', state.description);
    post.set('user', Parse.User.current());

    const acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setPublicWriteAccess(true);
    post.setACL(acl);

    post
      .save()
      .then((data) => {
        console.log('data is ', data);
        console.log('submitted successfully....');
        setState({ title: '', description: '' });
      })
      .catch((err) => {
        console.log('err is ', err.message);
        console.log('error....');
      });

};

console.log('state is ', state);
return (
<>
<CssBaseline />
<Container maxWidth='sm'>
<Box sx={{ height: '100vh' }}>

<h3>Add Record</h3>
<form onSubmit={handleSubmit}>
<div>
<TextField
fullWidth
required
label='Title'
name='title'
value={state.title}
onChange={(event) => {
handleChange('title', event.target.value);
}}
/>
</div>
<div style={{ marginTop: '25px' }}>
<TextField
fullWidth
required
label='Description'
name='description'
multiline={true}
rows={5}
value={state.description}
onChange={(event) => {
handleChange('description', event.target.value);
}}
/>
</div>
<div style={{ marginTop: '25px' }}>
<Button fullWidth variant='contained' type='submit'>
Submit
</Button>
</div>
</form>
</Box>
</Container>
</>
);
}

export default Add;

Master.jsx
import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Parse from 'parse';

function Master() {
const [state, setState] = React.useState({
results: null,
count: 0,
});
React.useEffect(() => {
const getRecords = () => {
const Test1 = Parse.Object.extend('Test1');
const query = new Parse.Query(Test1);
query.withCount();
query
.find()
.then((data) => {
setState((st) => {
return { ...st, ...data };
});
})
.catch((err) => {
console.log('err is ', err);
});
};
getRecords();
}, []);

console.log('state is ', state);
return (
<>
<CssBaseline />
<Container maxWidth='sm'>
<Box sx={{ height: '100vh' }}>

<h3>Master</h3>
<div>Total Records Found: {state.count}</div>
<div>
{state.results &&
state.results.length > 0 &&
state.results.map((rec) => {
return (
<Paper
key={rec.id}
elevation={3}
style={{ marginBottom: '15px', padding: '20px' }} >
<a href={`/detail/${rec.id}`}>{rec.get('title')}</a>{' '}
<a href={`/edit/${rec.id}`}>Edit</a> |
<a href={`/delete/${rec.id}`}>Delete</a>
</Paper>
);
})}
</div>
</Box>
</Container>
</>
);
}

export default Master;

Edit.jsx
import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Parse from 'parse';

function Edit(props) {
const id = props.match.params.id;
const [state, setState] = React.useState({
title: '',
description: '',
});

const handleChange = (name, value) => {
setState((st) => {
return { ...st, [name]: value };
});
};

const handleSubmit = (event) => {
event.preventDefault();

    console.log('submitting....');
    const Post = Parse.Object.extend('Test1');
    const post = new Post();
    post.id = id;

    post.set('title', state.title);
    post.set('description', state.description);

    post
      .save()
      .then((data) => {
        console.log('data is ', data);
        console.log('updated successfully....');
        props.history.push('/master');
      })
      .catch((err) => {
        console.log('err is ', err.message);
        console.log('error....');
      });

};

console.log('state is ', state);

React.useEffect(() => {
const Test1 = Parse.Object.extend('Test1');
const query = new Parse.Query(Test1);
query
.get(id)
.then((data) => {
setState((st) => {
return {
...st,
title: data.get('title'),
description: data.get('description'),
};
});
})
.catch((err) => {
console.log('err is ', err);
});
}, [id]);

return (
<>
<CssBaseline />
<Container maxWidth='sm'>
<Box sx={{ height: '100vh' }}>

<h3>Edit Record</h3>
<form onSubmit={handleSubmit}>
<div>
<TextField
fullWidth
required
label='Title'
name='title'
value={state.title}
onChange={(event) => {
handleChange('title', event.target.value);
}}
/>
</div>
<div style={{ marginTop: '25px' }}>
<TextField
fullWidth
required
label='Description'
name='description'
multiline={true}
rows={5}
value={state.description}
onChange={(event) => {
handleChange('description', event.target.value);
}}
/>
</div>
<div style={{ marginTop: '25px' }}>
<Button fullWidth variant='contained' type='submit'>
Submit
</Button>
</div>
</form>
</Box>
</Container>
</>
);
}

export default Edit;

Delete.jsx
import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Parse from 'parse';

function Delete(props) {
const id = props.match.params.id;
const [state, setState] = React.useState({
title: '',
description: '',
});

const handleSubmit = (event) => {
event.preventDefault();

    console.log('deleting....');
    const Post = Parse.Object.extend('Test1');
    const post = new Post();
    post.id = id;

    post
      .destroy()
      .then(() => {
        console.log('deleted successfully....');
        props.history.push('/master');
      })
      .catch((err) => {
        console.log('err is ', err.message);
        console.log('error....');
      });

};

console.log('state is ', state);

React.useEffect(() => {
const Test1 = Parse.Object.extend('Test1');
const query = new Parse.Query(Test1);
query
.get(id)
.then((data) => {
setState((st) => {
return {
...st,
title: data.get('title'),
description: data.get('description'),
};
});
})
.catch((err) => {
console.log('err is ', err);
});
}, [id]);

return (
<>
<CssBaseline />
<Container maxWidth='sm'>
<Box sx={{ height: '100vh' }}>

<h3>Delete Record</h3>
<form onSubmit={handleSubmit}>
<div>{state.title}</div>
<div style={{ marginTop: '25px' }}>{state.description}</div>
<div style={{ marginTop: '25px' }}>
<Button fullWidth variant='contained' type='submit'>
Delete This Record
</Button>
</div>
</form>
</Box>
</Container>
</>
);
}

export default Delete;

Detail.jsx

import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Parse from 'parse';

function Detail(props) {
const id = props.match.params.id;
const [state, setState] = React.useState({
title: '',
description: '',
});

console.log('state is ', state);

React.useEffect(() => {
const Test1 = Parse.Object.extend('Test1');
const query = new Parse.Query(Test1);
query
.get(id)
.then((data) => {
setState((st) => {
return {
...st,
title: data.get('title'),
description: data.get('description'),
};
});
})
.catch((err) => {
console.log('err is ', err);
});
}, [id]);

return (
<>
<CssBaseline />
<Container maxWidth='sm'>
<Box sx={{ height: '100vh' }}>

<h3>Detail Record</h3>

          <div>
            <strong>Title:</strong>
            <br />
            {state.title}
          </div>
          <div style={{ marginTop: '25px' }}>
            <strong>Description:</strong>
            <br />
            {state.description}
          </div>
        </Box>
      </Container>
    </>

);
}

export default Detail;

Sorting:

query.descending('createdAt'); // createdAt is the field name
or
query.ascending('createdAt');

Equal To:
query.equalTo('fieldName', 'value');
query.equalTo('title', 'fifth');

Most important:
query.matches('fieldname', 'value', 'i');
query.matches('title', 'rd', 'i');

query.skip(0); // 0, 1, Page2: 2, 3, Page3: 4, 5
query.limit(2);

For Live Query:
Go to parseserver index.js file

And add following:

liveQuery: {
classNames: ['Test1'],
},
ParseServer.createLiveQueryServer(httpServer);

React Side:
We need things: 1. Query 2. Subscription

const getRecordsLiveQuery = async () => {
const query = new Parse.Query('Test1');
query.equalTo('title', 'fifth');
const subscription = await query.subscribe();
subscription.on('open', () => {
console.log('subscription opened');
});
subscription.on('create', (object) => {
console.log('object created', object);
});
subscription.on('update', (object) => {
console.log('object updated', object);
});
subscription.on('enter', (object) => {
console.log('object entered', object);
});
subscription.on('leave', (object) => {
console.log('object left', object);
});
subscription.on('delete', (object) => {
console.log('object deleted', object);
});
subscription.on('close', () => {
console.log('subscription closed');
});
};

Create a new folder
run the following command inside that folder
C:/web/students
git clone https://github.com/manishkhanchandani/students.git .

useRef is used when you don't want to refresh the component.
useRef is always used to manipulate the input box.

Live Query

1. Create useRef variable
   const subscription = React.useRef(null);

2. Create a function where you will call livequery function
   const process = async () => {
   await getRecordsLiveQuery();
   };
   process();

3. Inside getRecordsLiveQuery function, add unsubscription part

if (subscription && subscription.current) {
await subscription.current.unsubscribe();
}

4. Change all subscription term to subscription.current inside getRecordsLiveQuery function,
   subscription.current = await query.subscribe();

We are building a simple application using livequery.

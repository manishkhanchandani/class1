import React from 'react';

function App() {
  const [name, setName] = React.useState('');
  const [age, setAge] = React.useState('');

  // console.log('name is ', name);
  // console.log('age is ', age);

  // this is version 1 of useEffect, do not use this
  // React.useEffect(() => {
  //   console.log('i am inside useEffect');
  // });

  // version 2, this will will call use effect only one time on page load
  // React.useEffect(() => {
  //   console.log('i will called only on page load');
  // }, []);

  // version 2, this will will call use effect only one time on page load
  // React.useEffect(() => {
  //   console.log('part 2');
  // }, []);

  // version 3,

  // React.useEffect(() => {
  //   console.log('1name is changed ', name);
  // }, [name]);

  // React.useEffect(() => {
  //   console.log('2age is changed ', age);
  // }, [age]);

  // React.useEffect(() => {
  //   console.log('3name and age is changed ', name, ', and ', age);
  // }, [name, age]);

  return (
    <div>
      <h1>useEffect Module</h1>
      <div>
        <h3>Name</h3>
        <input
          type='text'
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
      </div>
      <div>
        <h3>Age</h3>
        <input
          type='text'
          value={age}
          onChange={(event) => {
            setAge(event.target.value);
          }}
        />
      </div>
    </div>
  );
}

export default App;

import React from 'react';

const initialState = {
  firstName: '',
  lastName: '',
  address: '',
  dob: '',
  email: '',
  password: '',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'UPDATEFIRSTNAME':
      state = { ...state, firstName: action.payload };
      break;
    case 'UPDATELASTNAME':
      state = { ...state, lastName: action.payload };
      break;
    case 'UPDATEADDRESS':
      state = { ...state, address: action.payload };
      break;
    case 'UPDATEDOB':
      state = { ...state, dob: action.payload };
      break;
    case 'UPDATEFIELD':
      state = { ...state, ...action.payload };
      break;
    default:
      break;
  }
  return state;
};

function Example1() {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  console.log('state: ', state);
  return (
    <div>
      <h1>My Data</h1>
      <form action='/form/submit' method='get' autoComplete='off'>
        <div>
          <input
            type='email'
            id='abc123'
            name='Email'
            placeholder='First Name'
            autoComplete='email'
          />
        </div>
        <div>
          <input
            type='password'
            id='pass123'
            name='Password'
            placeholder='Password'
            autoComplete='new-password'
          />
        </div>
        <input type='submit' value='Submit' />
      </form>
      <form autoComplete='off'>
        <input autoComplete='off' type='hidden' />
        <div>
          FirstName:{' '}
          <input
            autoComplete='off'
            type='text'
            name='email'
            value={state.email}
            onChange={(e) => {
              //dispatch({ type: 'UPDATEFIRSTNAME', payload: e.target.value });
              dispatch({
                type: 'UPDATEFIELD',
                payload: { [e.target.name]: e.target.value },
              });
            }}
          />
        </div>
        <div>
          LastName:{' '}
          <input
            autoComplete='off'
            type='password'
            name='password'
            value={state.password}
            onChange={(e) => {
              //dispatch({ type: 'UPDATELASTNAME', payload: e.target.value });
              dispatch({
                type: 'UPDATEFIELD',
                payload: { [e.target.name]: e.target.value },
              });
            }}
          />
        </div>
      </form>
    </div>
  );
}

export default Example1;

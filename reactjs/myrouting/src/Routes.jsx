import React from 'react';
import { BrowserRouter, Switch, Route, Redirect, Link } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import Logout from './Logout';
import Categories from './Categories';
import Register from './Register';
import NotFound from './NotFound';
import Example1 from './Example1';

function Routes() {
  return (
    <div>
      <BrowserRouter>
        <h1>My Website</h1>
        <div>
          <Link to='/login'>Login</Link> | <Link to='/'>Home</Link> |{' '}
          <Link to='/logout'>Logout</Link> |{' '}
          <Link to='/register'>Register</Link>|{' '}
          <Link to='/categories'>Categories</Link>|{' '}
          <Link to='/example1'>Example1</Link>
          <hr />
        </div>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/notfound' exact component={NotFound} />
          <Route
            path='/login'
            exact
            render={(matchingProps) => {
              return <Login {...matchingProps} />;
            }}
          />
          <Route path='/logout' exact component={Logout} />
          <Route path='/register' exact component={Register} />
          <Route path='/example1' exact component={Example1} />
          <Route
            path='/categories/page/:pageId/max/:maxId'
            exact
            component={Categories}
          />
          <Route path='/categories/:categoryId' exact component={Categories} />
          <Route path='/categories' exact component={Categories} />
          <Redirect to='/notfound' />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default Routes;

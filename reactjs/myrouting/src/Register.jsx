import React from 'react';

function Register(props) {
  console.log('Register Props are: ', props);
  const handleSubmit = () => {
    // take form data, and save in database
    // we need to redirect user to home page or login page
    console.log('props: ', props);
    props.history.push('/');
  };
  return (
    <div>
      Register
      <button onClick={handleSubmit}>Register</button>
    </div>
  );
}

export default Register;

import React from 'react';
//useReducer
const initialState = {
  count: 0,
  name: '',
};
/*
action= {
  type: 'one',
  payload: 'jack'
}
*/

const reducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      state = { ...state, count: state.count + 1 };
      break;
    case 'DECREMENT':
      state = { ...state, count: state.count - 1 };
      break;
    case 'UPDATENAME':
      state = { ...state, name: action.payload };
      break;
    default:
      break;
  }
  return state;
};

export function Categories2() {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <div>
      <h1>Categories 2</h1>
      <div>Count is {state.count}</div>
      <div>Name is {state.name}</div>
      <div>
        <button
          onClick={() => {
            dispatch({
              type: 'INCREMENT',
            });
          }}
        >
          Increment
        </button>{' '}
        <button
          onClick={() => {
            dispatch({
              type: 'DECREMENT',
            });
          }}
        >
          Decrement
        </button>
      </div>
      <div>
        <div>Name: </div>
        <div>
          <input
            type='text'
            value={state.name}
            onChange={(e) => {
              dispatch({
                type: 'UPDATENAME',
                payload: e.target.value,
              });
            }}
          />
        </div>
      </div>
    </div>
  );
}

function Categories() {
  const [count, setCount] = React.useState(0);
  const [name, setName] = React.useState('');
  console.log('Categories count is ', count);
  console.log('Categories name is ', name);
  return (
    <div>
      <h1>Categories</h1>
      <div>
        <div>
          Count is <strong>{count}</strong>
        </div>
        <button
          onClick={() => {
            setCount(count + 1);
          }}
        >
          Increment
        </button>{' '}
        <button
          onClick={() => {
            setCount(count - 1);
          }}
        >
          Decrement
        </button>
      </div>
      <div>
        <div>Name: </div>
        <div>
          <input
            type='text'
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </div>
      </div>
      <hr />
      <Categories2 />
    </div>
  );
}

export default Categories;

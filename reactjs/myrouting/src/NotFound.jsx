import React from 'react';

function NotFound() {
  return (
    <div>
      <h3>NotFound</h3>
      <div>Page does not exist, please choose another page.</div>
    </div>
  );
}

export default NotFound;

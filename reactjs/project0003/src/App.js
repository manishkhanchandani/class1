import React from 'react';

// Basics
// 1. Decimal to Hex, we use num.toString(16);
// 2. To get part of substring we use str.substring(startIndex, endIndex);

function generateRandomColor() {
  const random = Math.random(); // 0 to 1 (1 is not included)
  const randomToString = random.toString(16);
  return '#' + randomToString.substring(2, 8);
}

function App() {
  const [hex, setHex] = React.useState('#ffffff');
  const handleClick = () => {
    const color = generateRandomColor();
    setHex(color);
    document.body.style.backgroundColor = color;
  };
  return (
    <div>
      <h1>Random Color Generator</h1>
      <div className='hex'>{hex}</div>
      <div className='buttons'>
        <button className='generate' onClick={handleClick}>
          Generate Color
        </button>
      </div>
    </div>
  );
}

export default App;

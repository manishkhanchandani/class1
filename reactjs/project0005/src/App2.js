import React from 'react';

// state variables

const Name = ({ state, setState }) => {
  return (
    <div>
      <div>My name is {state.name}</div>
      <div>
        <button
          onClick={() => {
            setState({ ...state, name: 'Tony' });
          }}
        >
          Change Name
        </button>
      </div>
    </div>
  );
};

const Age = ({ state, setState }) => {
  return (
    <div>
      <div>My age is {state.age}</div>

      <div>
        <button
          onClick={() => {
            setState({ ...state, age: 21 });
          }}
        >
          Change Age
        </button>
      </div>
    </div>
  );
};

const Gender = ({ state, setState }) => {
  return (
    <div>
      <div>My gender is {state.gender}</div>
      <div
        onClick={() => {
          setState({ ...state, gender: 'Female' });
        }}
      >
        <button>Change Gender</button>
      </div>
    </div>
  );
};

const Btns = ({ setState }) => {
  return (
    <div>
      <hr />
      <div>
        <button
          onClick={() => {
            setState((fV) => {
              return { ...fV, name: 'Tony2' };
            });
          }}
        >
          Change Name
        </button>
        <button
          onClick={() => {
            setState((fV) => {
              return { ...fV, age: 22 };
            });
          }}
        >
          Change Age
        </button>
        <button
          onClick={() => {
            setState((fV) => {
              return { ...fV, gender: 'Other' };
            });
          }}
        >
          Change Gender
        </button>
      </div>
    </div>
  );
};

function App() {
  const [state, setState] = React.useState({
    name: 'Jack',
    age: 34,
    gender: 'Male',
  });
  const [name, setName] = React.useState('');
  console.log('State Variables are: ', state);
  console.log('name Variables are: ', name);
  return (
    <div>
      <h1>My App</h1>
      <input
        type='text'
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <div>You typed this "{name}"</div>
      <hr />
      <Name state={state} setState={setState} />
      <Age state={state} setState={setState} />
      <Gender state={state} setState={setState} />
      <Btns setState={setState} />
    </div>
  );
}

export default App;

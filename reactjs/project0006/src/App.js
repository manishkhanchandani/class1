import React from 'react';
import MyForm from './widgets/MyForm';

// Email, Name, Password and Confirm Password
// Email, Password
function App() {
  const [formValues, setFormValues] = React.useState({
    email: '',
    name: '',
    password: '',
    confirm_password: '',
    aboutme: '',
    marital_status: '',
    fruits: [],
    hobbies: {},
    hobbies2: {
      reading: false,
      music: true,
    },
    gender: 'female',
    single_file: [],
    multiple_files: [],
    image_file: [],
    image_files: [],
    video_file: [],
    video_files: [],
  });

  const handleChange = (event) => {
    setFormValues((fV) => {
      return { ...fV, [event.target.name]: event.target.value };
    });
  };

  const handleValue = (name, value) => {
    setFormValues((fV) => {
      return { ...fV, [name]: value };
    });
  };

  console.log('formValues: ', formValues);

  return (
    <div>
      <div>
        <h3>Registration</h3>
        <div>
          <h3>Step 1</h3>
          <MyForm
            formValues={formValues}
            handleChange={handleChange}
            config={[
              {
                widget: 'textbox',
                name: 'email',
                label: 'Email',
                placeholder: 'Enter Email....',
                isRequired: true,
                type: 'email',
              },
              {
                widget: 'textbox',
                name: 'name',
                label: 'Name',
                placeholder: 'Enter Name...',
                isRequired: true,
                type: 'text',
              },
              {
                widget: 'textbox',
                name: 'password',
                label: 'Password',
                placeholder: 'Enter Password...',
                isRequired: true,
                type: 'password',
              },
              {
                widget: 'textbox',
                name: 'confirm_password',
                label: 'Confirm Password',
                placeholder: 'Enter Confirm Password...',
                isRequired: true,
                type: 'password',
              },
            ]}
          />
        </div>

        <div>
          <h3>Step 2</h3>
          <MyForm
            formValues={formValues}
            handleChange={handleChange}
            handleValue={handleValue}
            config={[
              {
                widget: 'textarea',
                name: 'aboutme',
                label: 'About Me',
                placeholder: 'Enter about myself....',
                isRequired: true,
              },
              {
                widget: 'select',
                name: 'marital_status',
                label: 'Marital Status',
                options: [
                  { key: 'married', label: 'Married' },
                  { key: 'single', label: 'Single' },
                  { key: 'divorced', label: 'Divorced' },
                  { key: 'widow', label: 'Widow' },
                ],
              },
              {
                widget: 'selectmultiple',
                name: 'fruits',
                label: 'Fruits',
                options: [
                  { key: 'mango', label: 'Mango' },
                  { key: 'lime', label: 'Lime' },
                  { key: 'grapefruit', label: 'Grapefruit' },
                  { key: 'cocunut', label: 'Coconut' },
                ],
              },
              {
                widget: 'checkbox',
                name: 'hobbies',
                label: 'Hobbies',
                options: [
                  { key: 'movies', label: 'Movies' },
                  { key: 'travelling', label: 'Travelling' },
                  { key: 'eating', label: 'Eating' },
                  { key: 'chess', label: 'Chess' },
                ],
              },
              {
                widget: 'checkbox',
                name: 'hobbies2',
                label: 'Hobbies 2',
                options: [
                  { key: 'travelling', label: 'Travelling' },
                  { key: 'drawing', label: 'Drawing' },
                  { key: 'music', label: 'Music' },
                  { key: 'reading', label: 'reading' },
                  { key: 'learning', label: 'Learning' },
                ],
              },
              {
                widget: 'radiobox',
                name: 'gender',
                label: 'Gender',
                options: [
                  { key: 'male', label: 'Male' },
                  { key: 'female', label: 'Female' },
                  { key: 'other', label: 'Other' },
                ],
              },
            ]}
          />
        </div>
      </div>
    </div>
  );
}

export default App;

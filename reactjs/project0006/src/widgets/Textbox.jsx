import React from 'react';

function Textbox({
  name,
  label,
  placeholder,
  isRequired,
  type,
  value,
  handleChange,
}) {
  return (
    <div>
      <div>
        <label>
          {label}: {isRequired && <span>*</span>}
        </label>
      </div>
      <div>
        <input
          type={type}
          name={name}
          placeholder={placeholder}
          required={isRequired}
          value={value}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}

export default Textbox;

import React from 'react';

function Textarea({
  name,
  label,
  placeholder,
  isRequired,
  value,
  handleChange,
}) {
  return (
    <div>
      <div>
        <label>
          {label}: {isRequired && <span>*</span>}
        </label>
      </div>
      <div>
        <textarea
          name={name}
          placeholder={placeholder}
          required={isRequired}
          value={value}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}

export default Textarea;

import React from 'react';
import Textbox from './Textbox';
import Textarea from './Textarea';
import Checkbox from './Checkbox';
import Radiobox from './Radiobox';
import SelectBox from './SelectBox';
import SelectMultiple from './SelectMultiple';
import Filebox from './Filebox';

function MyForm({ config, formValues, handleChange, handleValue }) {
  return (
    <div>
      {config.map((record, index) => {
        switch (record.widget) {
          case 'textbox':
            return (
              <Textbox
                key={index}
                name={record.name}
                label={record.label}
                placeholder={record.placeholder}
                isRequired={record.isRequired}
                type={record.type}
                value={formValues[record.name]}
                handleChange={handleChange}
              />
            );
          case 'textarea':
            return (
              <Textarea
                key={index}
                name={record.name}
                label={record.label}
                placeholder={record.placeholder}
                isRequired={record.isRequired}
                value={formValues[record.name]}
                handleChange={handleChange}
              />
            );
          case 'select':
            return <SelectBox />;
          case 'selectmultiple':
            return <SelectMultiple />;
          case 'checkbox':
            return (
              <Checkbox
                key={index}
                name={record.name}
                label={record.label}
                options={record.options}
                value={formValues[record.name]}
                handleValue={handleValue}
              />
            );
          case 'radiobox':
            return (
              <Radiobox
                key={index}
                name={record.name}
                label={record.label}
                options={record.options}
                value={formValues[record.name]}
                handleChange={handleChange}
              />
            );
          case 'filebox':
            return <Filebox />;
          default:
            break;
        }
        return <div>To do </div>;
      })}
    </div>
  );
}

export default MyForm;

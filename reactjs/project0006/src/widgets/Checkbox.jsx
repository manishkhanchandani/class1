import React from 'react';

function Checkbox({ name, label, options, value, handleValue }) {
  return (
    <div>
      <div>{label}</div>
      <div>
        {options.map((option, index) => {
          return (
            <div key={name + index}>
              <label>{option.label}</label>
              <input
                type='checkbox'
                name={option.key}
                checked={value[option.key]}
                onChange={(e) => {
                  handleValue(name, {
                    ...value,
                    [e.target.name]: e.target.checked,
                  });
                }}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Checkbox;

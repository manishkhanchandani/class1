import React from 'react';

function Radiobox({ name, label, value, options, handleChange }) {
  return (
    <div>
      <div>{label}</div>
      <div>
        {options.map((option, index) => {
          return (
            <div key={name + index}>
              <label>{option.label}</label>
              <input
                type='radio'
                name={name}
                value={option.key}
                onChange={handleChange}
                checked={option.key === value}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Radiobox;

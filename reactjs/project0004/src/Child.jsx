import React from 'react';
import PropTypes from 'prop-types';

// component
const Child = (props) => {
  console.log('child props are ', props);
  return (
    <div>
      <h3>Child Component</h3>
      <div>My age is {props.age}</div>
      <div>My gender is {props.gender}</div>
    </div>
  );
};

Child.propTypes = {
  age: PropTypes.number,
  gender: PropTypes.string,
};

Child.defaultProps = {
  age: 45,
  gender: 'Male',
};

export default Child;

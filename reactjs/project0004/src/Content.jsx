import React from 'react';

const Content = () => {
  const name = 'Tony';
  const age = 23;
  const isMember = false;
  const hobbies = ['chess', 'movies', 'eating'];
  const people = ['jack baur', 'tony almeda', 'nina mayers', 'xyz'];
  const addresses = [
    { id: 'a', city: 'fremont', state: 'ca', country: 'usa' },
    { id: 'b', city: 'mumbai', state: 'mh', country: 'india' },
    { id: 'c', city: 'austin', state: 'tx', country: 'usa' },
  ];
  // objects cannot be displayed on the page.
  return (
    <div>
      This is content. My name is <strong>{name}</strong>. My age is {age}. Is
      Member: {isMember ? 'Yes' : 'No'}. Hobbies: {hobbies}.
      <ul>
        {hobbies.map((record) => {
          return <li key={record}>{record}</li>;
        })}
      </ul>
      <ul>
        {people.map((record) => {
          return <li key={record}>{record}</li>;
        })}
      </ul>
      <ul>
        {addresses.map((record) => {
          return (
            <li key={record.id}>
              City is {record.city}, State is {record.state} and country is{' '}
              {record.country}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Content;

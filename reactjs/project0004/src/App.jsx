import React from 'react';
import Parent from './Parent';

// props
// props are read only
// you should not change the value of props
// prop-types (npm install --save prop-types)

// component
const App = () => {
  const name = 'Jack';
  const age = 34;
  const gender = 'Male';
  return (
    <div>
      <h1>Main Component</h1>
      <Parent
        name={name}
        age={age}
        gender={gender}
        is_married={true}
        hobbies={['chess', 'movies', 'eating']}
      />
    </div>
  );
};

export default App;

import React from 'react';
import Header, { SubHeader, SubHeader2 } from './Header';
import Content from './Content';
import Footer from './Footer';

const App = () => {
  return (
    <div>
      <Header />
      <SubHeader />
      <SubHeader2 />
      <Content />
      <Footer />
    </div>
  );
};

export default App;

import React from 'react';
import Child from './Child';

// component
const Parent = (props) => {
  console.log('props are ', props);
  return (
    <div>
      <h2>Parent Component</h2>
      <div>My name is {props.name}</div>
      <Child gender={props.gender} age={props.age} {...props} />
    </div>
  );
};

export default Parent;

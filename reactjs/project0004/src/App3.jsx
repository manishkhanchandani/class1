import React from 'react';

const Content = (props) => {
  return (
    <div>
      <div>Name: {props.name}</div>
      <div>Age: {props.age}</div>
      <div>Gender: {props.gender}</div>
      <hr />
    </div>
  );
};

const App = () => {
  return (
    <div>
      <h1>Exercise 2</h1>
      <Content name='Jack Baur' age={34} gender='male' />
      <Content name='Tony' age={23} gender='male' />
      <Content name='Nina' age={26} gender='female' />
      <Content name='xyz' age={33} gender='female' />
    </div>
  );
};

export default App;

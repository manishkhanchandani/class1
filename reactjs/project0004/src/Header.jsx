import React from 'react';

const Header = () => {
  return <h1>Header</h1>;
};

export const SubHeader = () => {
  return <h1>Sub Header</h1>;
};

export const SubHeader2 = () => {
  return <h1>Sub Header 2</h1>;
};

export default Header;

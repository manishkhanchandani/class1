import React from 'react';

const Display = (props) => {
  return (
    <div>
      <div>Firstname: {props.firstName}</div>
      <div>Lastname: {props.lastName}</div>
    </div>
  );
};

const App = () => {
  const people = [
    { id: 1, firstName: 'jack', lastName: 'baur' },
    { id: 2, firstName: 'tony', lastName: 'alameda' },
  ];
  return (
    <div>
      <h1>Exercise 3</h1>
      {people.map((record) => {
        return (
          <Display
            key={record.id}
            firstName={record.firstName}
            lastName={record.lastName}
          />
        );
      })}
    </div>
  );
};

export default App;

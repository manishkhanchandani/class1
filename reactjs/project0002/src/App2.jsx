import React, { useState, useEffect } from 'react';

function App() {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [confirmedPhoneNumber, setConfirmedPhoneNumber] = useState('');
  const [buttonActive, setButtonAction] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (phoneNumber && confirmedPhoneNumber) {
      setButtonAction(true);
    }
  }, [phoneNumber, confirmedPhoneNumber]);

  const handleChangePhoneNumber = (value) => {
    if (value.length === 1 && value === ' ') {
      return;
    } else {
      setPhoneNumber(value);
      console.log('phone number', value);
    }
  };

  const handleSendConfirmPhoneNumber = (event) => {
    if (event.target.value.length === 1 && event.target.value === ' ') {
      return;
    } else {
      setConfirmedPhoneNumber(event.target.value);
    }

    if (event.target.value.length > 0 && phoneNumber.length > 0) {
      setButtonAction(true);
    } else {
      setButtonAction(false);
    }
  };

  const handleNextButtonStyle = () => {
    let btnStyle = 'buttonBox buttonMarginTop58 buttonMarginTopNameMobile';

    if (isLoading) {
      btnStyle =
        'buttonMarginTop58 buttonBox buttonMarginTopNameMobile not-allowed spinner';
    }

    if (buttonActive) {
      btnStyle = 'buttonBoxActive buttonMarginTop58 buttonMarginTopNameMobile';
    }

    return btnStyle;
  };
  const handleSkipButtonStyle = () => {
    let btnStyle = 'skipButton';

    if (isLoading) {
      btnStyle = 'skipButton not-allowed spinner';
    }
    return btnStyle;
  };

  const handleClickSendConfirmationCode = () => {
    setButtonAction(false);
    setIsLoading(true);

    if (
      phoneNumber.length <= 9 ||
      confirmedPhoneNumber.length <= 9 ||
      phoneNumber === ' ' ||
      confirmedPhoneNumber === ' ' ||
      phoneNumber.length >= 16 ||
      confirmedPhoneNumber.length >= 16
    ) {
      setErrorMessage(
        'Please enter valid 10 digit Phone number and confirm phone number.'
      );
      setIsLoading(false);
    } else if (phoneNumber !== confirmedPhoneNumber) {
      setErrorMessage('Please make sure the passwords match.');
      setIsLoading(false);
    } else {
      goToVerifyCodeScreen(phoneNumber);
    }
  };

  const skipButtonClick = () => {
    const val = '';
    setIsLoading(true);
    setPhoneNumber(val);
    setConfirmedPhoneNumber(val);
    setErrorMessage('');
    goToVerifyCodeScreen(val);
  };
  const goToVerifyCodeScreen = (val) => {
    console.log('phoneNumber========', val);
    setErrorMessage('');
  };

  return (
    <div className='webTop'>
      <div className='webContainer'>
        <div className='webMaster'>
          {isLoading ? <div className='transparentOverLay'></div> : null}

          <div className='welcomeContainerWeb'>
            <div className='innerScreenMasterWeb'></div>
            <div
              className='card cardHeight mainContainer'
              style={{ textAlign: 'center' }}
            >
              <div className='fullWidth'>
                <h6 className='screenTitle'>Sign Up</h6>
                <div className='SignUpBox'>
                  <div className='inputBoxCol'>
                    <label className='marginBottom'>Phone Number</label>
                    <div className='inputBoxCol marginBottomSpace'>
                      <input
                        style={{ marginBottom: 10, marginTop: 10 }}
                        placeholder='Enter Mobile Number'
                        type='number'
                        onChange={(e) =>
                          handleChangePhoneNumber(e.target.value)
                        }
                        value={phoneNumber}
                      ></input>
                    </div>
                    <div className='input_label_confirm marginLeftNone'></div>
                    <div className='inputBoxCol'>
                      <input
                        style={{ marginBottom: 10 }}
                        placeholder='Confirm Mobile Number'
                        type='number'
                        onChange={handleSendConfirmPhoneNumber}
                        value={confirmedPhoneNumber}
                      ></input>
                    </div>
                    <div className='errMessage'>
                      {errorMessage !== '' ? 'Error . ' : null}
                      <span>{errorMessage}</span>
                    </div>
                    <div className='buttonCol'>
                      <button
                        style={{
                          marginTop: 10,
                          marginBottom: 20,
                          background: 'darkblue',
                          color: '#fff',
                          paddingTop: 6,
                          paddingBottom: 6,
                          paddingLeft: 10,
                          paddingRight: 10,
                          cursor: 'pointer',
                        }}
                        disabled={!buttonActive}
                        className={handleNextButtonStyle()}
                        onClick={handleClickSendConfirmationCode}
                      >
                        Send Confirmation Code
                      </button>
                      <div className='fullWidh'>
                        <span
                          style={{ cursor: 'pointer' }}
                          className={handleSkipButtonStyle()}
                          onClick={skipButtonClick}
                        >
                          Skip
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;

import React from 'react';

function generateRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function App() {
  // state variable
  const [number, setNumber] = React.useState(0);
  const handleClick = () => {
    const x = generateRandomNumber(1, 100000); // 45
    setNumber(x);
  };

  
  return (
    <div>
      <h1>Random Number Generator</h1>
      <div>Number: {number}</div>
      <div>
        <button onClick={handleClick}>Generate New Random Number</button>
      </div>
    </div>
  );
}

export default App;

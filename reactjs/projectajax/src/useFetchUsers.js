import React from 'react';

// CUSTOM HOOK
function useFetchUsers() {
  const [users, setUsers] = React.useState(null);

  // whenever you want to do something on page load, use react use effect
  React.useEffect(() => {
    const url = 'http://jsonplaceholder.typicode.com/users';
    console.log('url is', url);
    fetch(url)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setUsers(data);
      })
      .catch((err) => {
        console.log('err is ', err);
      });
  }, []);

  return users;
}

export default useFetchUsers;

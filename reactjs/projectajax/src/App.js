import React from 'react';
import useFetchUsers from './useFetchUsers';

function App() {
  const users = useFetchUsers();
  console.log('users is ', users);

  return (
    <div>
      <h1>Users</h1>
      {users && users.length > 0 && (
        <ul>
          {users.map((u) => {
            return (
              <li key={u.id}>
                <div>{u.name}</div>
                <div>{u.phone}</div>
                <div>{u.email}</div>
                <br />
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
}

export default App;

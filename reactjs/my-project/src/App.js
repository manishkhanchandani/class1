import React from 'react';

function App() {
  // state variable
  const [num, setNum] = React.useState(0);
  const name = 'xyz';

  const [color, setColor] = React.useState('#000000');
  return (
    <div className='container'>
      <h1>My World {name}</h1>
      <div>My Number: {num}</div>
      <button
        onClick={() => {
          setNum(num + 1);
        }}
      >
        Increment
      </button>
      <div>My Color: {color}</div>
      <button
        onClick={() => {
          setColor('#ff0000');
        }}
      >
        Change Color
      </button>
    </div>
  );
}

export default App;

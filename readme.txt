Step 1:
Open Visual Studio Code,

Click
Open Folder

Then choose any folder where you want to save the files.

Step 2: create folders
css
	basic
	advance
	flex
	grid
db	
	mongo
	mysql
	parseserver
ds
js
nodejs
reactjs

Step 3:
Create a function with no tab or space
and then click the save button, it should format it automatically.

Step 4:
Install the extension Prettier - Code Formatter by Prettier

Go to file -> preference -> settings
and choose 
defaultFormatter to Prettier - code Formatter

Format on Save, make it checked


Prettier - search

1. Arrow Parens
always
2. Bracket Spacing - checked
3. config path - empty
4. Embedded language formatting - auto
5. Prettier Enable - checked
6. Enable debug logs - checked
7. End of line - if
8. html whitespace sensitivity - css
9. ignore path - empty
10. Jsx Single Quote - checked,
11. print width - 80
12. pose wrap: preserve
13. quote props - as needed
14. prettier semi - checked
15. prettier single quote checked
16. tab width 2
17. trailing comma - es5
18. editor config - checked

word wrap
and make it on for editor: Word wrap

Install
ES7 React/Redux/GraphQL/React-Native snippets

dsznajder

Path Intellisense
Christian Kohler

Sonarlint
SonarSource

https://www.java.com/en/download/manual.jsp

"sonarlint.ls.javaHome": "C:\\Program Files\\Java\\jre1.8.0_131"

EsLint
npm install -g eslint
Create config file
.eslintrc by running following line

eslint --init


Downgrade npm to version 6

npm install -g npm@6.14.15

eslint file
https://gist.github.com/adrianhall/70d63d225e536b4563b2


var OFF = 0, WARN = 1, ERROR = 2;
module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    // // Possible Errors (overrides from recommended set)
    // 'no-extra-parens': ERROR,
    // 'no-unexpected-multiline': ERROR,
    // // All JSDoc comments must be valid
    // 'valid-jsdoc': [
    //     ERROR,
    //     {
    //         requireReturn: false,
    //         requireReturnDescription: false,
    //         requireParamDescription: true,
    //         prefer: {
    //             return: 'returns',
    //         },
    //     },
    // ],
    // // Best Practices
    // // Allowed a getter without setter, but all setters require getters
    // 'accessor-pairs': [
    //     ERROR,
    //     {
    //         getWithoutSet: false,
    //         setWithoutGet: true,
    //     },
    // ],
    // 'block-scoped-var': WARN,
    // 'consistent-return': ERROR,
    // curly: ERROR,
    // 'default-case': WARN,
    // // the dot goes with the property when doing multiline
    // 'dot-location': [WARN, 'property'],
    // 'dot-notation': WARN,
    // eqeqeq: [ERROR, 'smart'],
    // 'guard-for-in': WARN,
    // 'no-alert': ERROR,
    // 'no-caller': ERROR,
    // 'no-case-declarations': WARN,
    // 'no-div-regex': WARN,
    // 'no-else-return': WARN,
    // 'no-empty-label': WARN,
    // 'no-empty-pattern': WARN,
    // 'no-eq-null': WARN,
    // 'no-eval': ERROR,
    // 'no-extend-native': ERROR,
    // 'no-extra-bind': WARN,
    // 'no-floating-decimal': WARN,
    // 'no-implicit-coercion': [
    //     WARN,
    //     {
    //         boolean: true,
    //         number: true,
    //         string: true,
    //     },
    // ],
    // 'no-implied-eval': ERROR,
    // 'no-invalid-this': ERROR,
    // 'no-iterator': ERROR,
    // 'no-labels': WARN,
    // 'no-lone-blocks': WARN,
    // 'no-loop-func': ERROR,
    // 'no-magic-numbers': OFF,
    // 'no-multi-spaces': ERROR,
    // 'no-multi-str': WARN,
    // 'no-native-reassign': ERROR,
    // 'no-new-func': ERROR,
    // 'no-new-wrappers': ERROR,
    // 'no-new': ERROR,
    // 'no-octal-escape': ERROR,
    // 'no-param-reassign': ERROR,
    // 'no-process-env': WARN,
    // 'no-proto': ERROR,
    // 'no-redeclare': ERROR,
    // 'no-return-assign': ERROR,
    // 'no-script-url': ERROR,
    // 'no-self-compare': ERROR,
    // 'no-throw-literal': ERROR,
    // 'no-unused-expressions': ERROR,
    // 'no-useless-call': ERROR,
    // 'no-useless-concat': ERROR,
    // 'no-void': WARN,
    // 'no-with': WARN,
    // radix: WARN,
    // 'vars-on-top': ERROR,
    // // Enforces the style of wrapped functions
    // 'wrap-iife': [ERROR, 'outside'],
    // yoda: ERROR,
    // // Strict Mode - for ES6, never use strict.
    // strict: [ERROR, 'never'],
    // // Variables
    // 'init-declarations': [ERROR, 'always'],
    // 'no-catch-shadow': WARN,
    // 'no-delete-var': ERROR,
    // 'no-label-var': ERROR,
    // 'no-shadow-restricted-names': ERROR,
    // 'no-shadow': WARN,
    // // We require all vars to be initialized (see init-declarations)
    // // If we NEED a var to be initialized to undefined, it needs to be explicit
    // 'no-undef-init': OFF,
    // 'no-undef': ERROR,
    // 'no-undefined': OFF,
    // 'no-unused-vars': WARN,
    // // Disallow hoisting - let & const don't allow hoisting anyhow
    // 'no-use-before-define': ERROR,
    // // Node.js and CommonJS
    // 'callback-return': [WARN, ['callback', 'next']],
    // 'global-require': ERROR,
    // 'handle-callback-err': WARN,
    // 'no-mixed-requires': WARN,
    // 'no-new-require': ERROR,
    // // Use path.concat instead
    // 'no-path-concat': ERROR,
    // 'no-process-exit': ERROR,
    // 'no-restricted-modules': OFF,
    // 'no-sync': WARN,
    // // ECMAScript 6 support
    // 'arrow-body-style': [ERROR, 'always'],
    // 'arrow-parens': [ERROR, 'always'],
    // 'arrow-spacing': [ERROR, { before: true, after: true }],
    // 'constructor-super': ERROR,
    // 'generator-star-spacing': [ERROR, 'before'],
    // 'no-arrow-condition': ERROR,
    // 'no-class-assign': ERROR,
    // 'no-const-assign': ERROR,
    // 'no-dupe-class-members': ERROR,
    // 'no-this-before-super': ERROR,
    // 'no-var': WARN,
    // 'object-shorthand': [WARN, 'never'],
    // 'prefer-arrow-callback': WARN,
    // 'prefer-spread': WARN,
    // 'prefer-template': WARN,
    // 'require-yield': ERROR,
    // // Stylistic - everything here is a warning because of style.
    // 'array-bracket-spacing': [WARN, 'never'],
    // 'block-spacing': [WARN, 'always'],
    // 'brace-style': [WARN, '1tbs', { allowSingleLine: false }],
    // camelcase: WARN,
    // 'comma-spacing': [WARN, { before: false, after: true }],
    // 'comma-style': [WARN, 'last'],
    // 'computed-property-spacing': [WARN, 'never'],
    // 'consistent-this': [WARN, 'self'],
    // 'eol-last': WARN,
    // 'func-names': WARN,
    // 'func-style': [WARN, 'declaration'],
    // 'id-length': [WARN, { min: 2, max: 32 }],
    // indent: [WARN, 4],
    // 'jsx-quotes': [WARN, 'prefer-double'],
    // 'linebreak-style': [WARN, 'unix'],
    // 'lines-around-comment': [WARN, { beforeBlockComment: true }],
    // 'max-depth': [WARN, 8],
    // 'max-len': [WARN, 132],
    // 'max-nested-callbacks': [WARN, 8],
    // 'max-params': [WARN, 8],
    // 'new-cap': WARN,
    // 'new-parens': WARN,
    // 'no-array-constructor': WARN,
    // 'no-bitwise': OFF,
    // 'no-continue': OFF,
    // 'no-inline-comments': OFF,
    // 'no-lonely-if': WARN,
    // 'no-mixed-spaces-and-tabs': WARN,
    // 'no-multiple-empty-lines': WARN,
    // 'no-negated-condition': OFF,
    // 'no-nested-ternary': WARN,
    // 'no-new-object': WARN,
    // 'no-plusplus': OFF,
    // 'no-spaced-func': WARN,
    // 'no-ternary': OFF,
    // 'no-trailing-spaces': WARN,
    // 'no-underscore-dangle': WARN,
    // 'no-unneeded-ternary': WARN,
    // 'object-curly-spacing': [WARN, 'always'],
    // 'one-var': OFF,
    // 'operator-assignment': [WARN, 'never'],
    // 'operator-linebreak': [WARN, 'after'],
    // 'padded-blocks': [WARN, 'never'],
    // 'quote-props': [WARN, 'as-needed'],
    // quotes: [WARN, 'single'],
    // 'require-jsdoc': [
    //     OFF,
    //     {
    //         require: {
    //             FunctionDeclaration: true,
    //             MethodDefinition: true,
    //             ClassDeclaration: false,
    //         },
    //     },
    // ],
    // 'semi-spacing': [WARN, { before: false, after: true }],
    // semi: [ERROR, 'always'],
    // 'sort-vars': OFF,
    // 'space-after-keywords': [WARN, 'always'],
    // 'space-before-blocks': [WARN, 'always'],
    // 'space-before-function-paren': [WARN, 'never'],
    // 'space-before-keywords': [WARN, 'always'],
    // 'space-in-parens': [WARN, 'never'],
    // 'space-infix-ops': [WARN, { int32Hint: true }],
    // 'space-return-throw-case': ERROR,
    // 'space-unary-ops': ERROR,
    // 'spaced-comment': [WARN, 'always'],
    // 'wrap-regex': WARN,
  },
};

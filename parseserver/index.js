const express = require('express');
const ParseServer = require('parse-server').ParseServer;
const ParseDashboard = require('parse-dashboard');
const path = require('path');

const app = express();

// configuration for parse server
const dbConn = 'mongodb://localhost:27017/project1';

const APP_ID = 'appId';
const MASTER_KEY = 'masterKey';
const REST_API_KEY = 'restApiKey';
const SERVER_URL = 'http://localhost:1337/parse';

const api = ParseServer({
  databaseURI: dbConn,
  cloud: __dirname + '/cloud/main.js',
  appId: APP_ID,
  masterKey: MASTER_KEY,
  restApiKey: REST_API_KEY,
  serverURL: SERVER_URL,
  liveQuery: {
    classNames: ['Test1', 'Test2Todo'],
  },
});

app.use('/parse', api);

// configuration for dashboard
const dashboard = new ParseDashboard(
  {
    apps: [
      {
        serverURL: SERVER_URL,
        appId: APP_ID,
        masterKey: MASTER_KEY,
        appName: 'My World',
      },
    ],
    users: [
      {
        user: 'manish',
        pass: 'password',
      },
    ],
    trustProxy: 1,
  },
  {
    allowInsecureHTTP: true,
  }
);

app.use('/dashboard', dashboard);

app.use('/public', express.static(path.join(__dirname, '/public')));

app.get('/test', (req, res) => {
  res.send('parse server test is running');
});

const httpServer = require('http').createServer(app);
httpServer.listen(1337, () => {
  console.log('Server Started');
});

ParseServer.createLiveQueryServer(httpServer);

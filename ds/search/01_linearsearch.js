/*
 * var arr = [5, 8, 1, 100, 12, 3, 14];
 * var value = 100;
 * How do we search in linearSearch
 * This function will accept an array and a value
 * Loop through the array and check if the current array element is equal to the value.
 * If it is, return the index at which the element is found.
 * If it is not found, we return -1;
 */

function linearSearch(arr, search) {
  // if array is empty, return -1
  if (!arr || arr.length === 0) {
    return -1;
  }
  // for loop to process all the elements of the array
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === search) {
      return i;
    }
  }
  // if element is equal to the search element, if it is eqal we return index
  // return -1;
  return -1;
}

console.log(linearSearch([10, 15, 20, 25, 30], 15)); // 1
console.log(linearSearch([9, 8, 7, 6, 5, 4, 3, 3, 2, 1, 0], 4)); // 5
console.log(linearSearch([100], 100)); // 0
console.log(linearSearch([1, 2, 3, 4, 5], 6)); // -1
console.log(linearSearch([9, 8, 7, 6, 5, 4, 3, 2, 1, 0], 10)); // -1
console.log(linearSearch([100], 200)); // -1

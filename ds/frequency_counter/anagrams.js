/*
Anagrams

str1 = cat
str2 = tac
this is anagram

str1 = fat
str2 = cat
not an anagram

str1 = ccat
str2 = atcc
this is anagram

every thing is lower case, with no spaces, not special characters

Step 1: Create Character Map for the each string.

obj1 = {
        h: 1,
        e: 1,
        l: 2,
        o: 1,
    }

obj2 = {
        o: 1,
        e: 1,
        l: 2,
        h: 1,
    }
Step 2: 
    loop through obj1

Step 3:
    check if each character (key) of obj1 has same value in obj2.
*/

function validAnagram(str1, str2) {
  if (str1.length !== str2.length) {
    return false;
  }
  const obj1 = {};
  const obj2 = {};
  // loop str1 to create character map
  for (let i of str1) {
    if (obj1[i]) {
      obj1[i]++;
    } else {
      obj1[i] = 1;
    }
  }
  // loop str2 to create character map
  for (let i of str2) {
    if (obj2[i]) {
      obj2[i]++;
    } else {
      obj2[i] = 1;
    }
  }

  console.log('obj1: ', obj1);
  console.log('obj2: ', obj2);

  for (let char in obj1) {
    let val1 = obj1[char];
    let val2 = obj2[char];
    if (val1 !== val2) {
      return false;
    }
  }

  return true;
}

const val = validAnagram('hello', 'oelhl'); // true
console.log('val is ', val);

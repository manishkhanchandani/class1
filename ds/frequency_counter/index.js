let arr;
arr = ['a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'c'];

// build a character map by passing array in it and output is object with number of times that key exists in an array
function characterMap1(ar) {
  const obj = {};
  for (let i of ar) {
    if (obj[i]) {
      obj[i]++;
    } else {
      obj[i] = 1;
    }
  }
  return obj;
}

let x = characterMap1(arr);

console.log(x);

// input is an object, loop through object and find the maximum number and return it.
function maxNumber(input) {
  let max = 0; // -Infinity;
  for (let i in input) {
    // value: input[i] and key is i
    if (input[i] > max) {
      max = input[i];
    }
  }
  return max;
}

const y = maxNumber(x);
console.log('max is ', y);

function minNumber(input) {
  console.log('input is ', input);
  return 1;
}

const z = minNumber(x);
console.log('min is ', y);

// return the character with max value
function maxChar(input) {
  console.log('input is ', input);
  return 1;
}

const k = maxChar(x);
console.log('maxChar is ', k);

function minChar(input) {
  console.log('input is ', input);
  return 1;
}

const l = minChar(x);
console.log('minChar is ', l);

arr = [5, 7, 3, 2, 9];

function characterMap2(ar) {
  const obj = {};

  for (let i = 0; i < ar.length; i++) {
    let val = ar[i];
    obj[val] = i;
  }

  return obj;
}

const a = characterMap2(arr);
console.log('a is ', a);
/*obj = {
5: 0,
7: 1,
3: 2,
2: 3,
9: 4
}*/


arr = ['a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'c', 'd', 'd'];
function manipulate(input) {
  let obj = {};
  for (let i in input) {
    console.log('i is ', i);
  }
  return obj;
}

x = characterMap1(arr);

console.log(x);

const g = manipulate(x);

/*
obj = {
    '2': ['a', 'd'],
    '3': ['b'],
    '4': ['c']
}
*/

// character map
// 1. max char / min char
// 2. Duplicates
// 3. Same Frequency
// 4. Anagrams
// 5. Compare arrays
// 6. Two Sum

/* input will be array or string
var arr = ['a', 'b', 'c', 'a'];
var str = 'abc';

obj = {
a: 2,
b: 1,
c: 1
};
*/
function buildCharMap(input) {
    const obj = {};
    for (let i of input) { // for-of
        if (obj[i]) {
            obj[i]++;
        } else {
            obj[i] = 1;
        }
    }
    return obj;
}

var x1 = buildCharMap(['a', 'b', 'c', 'a']);
var x2 = buildCharMap(['a', 'b', 'b', 'c', 'c', 'c', 'd', 'd', 'd', 'd']);
var x3 = buildCharMap('helloworld');
var x4 = buildCharMap('abccbaabccbbbbbbb');
var x5 = buildCharMap('abc');

STEP 1: BUILD CHARACTER MAP.
already done above

STEP 2: WRITE DIFFERENT LOGIC BASED ON REQUIREMENT.

function maxChars(input) {
    var max = -Infinity;
    var maxC = '';
    for (var i in input) {
        // i is nothing but key like a, b, c
        // input[i] is nothing but value
        var val = input[i];
        // compare this value with max value and if this is greater then change the max value
        if (val > max) {
            max = val;
            maxC = i;
        }
    }

    return maxC;
}


var x1 = buildCharMap(['a', 'b', 'c', 'a']);
console.log(maxChars(x1));
var x2 = buildCharMap(['a', 'b', 'b', 'c', 'c', 'c', 'd', 'd', 'd', 'd']);
console.log(maxChars(x2));
var x3 = buildCharMap('helloworld');
console.log(maxChars(x3));
var x4 = buildCharMap('abccbaabccbbbbbbb');
console.log(maxChars(x4));

function minChars(input) {
    var min = Infinity;
    var minC = '';
    for (var i in input) {
        // i is nothing but key like a, b, c
        // input[i] is nothing but value
        var val = input[i];
        // compare this value with max value and if this is greater then change the max value
        if (val < min) {
            min = val;
            minC = i;
        }
    }
​
    return minC;
}


// return true or false,
// if it is duplicate, we return true, else we return false
function isDuplicate(input) {
    for (var i in input) {
        var val = input[i];
        if (val > 1) {
            return true;
        }
    }

    return false;
}

isDuplicate(x1);
isDuplicate(x2);
isDuplicate(x3);
isDuplicate(x4);
isDuplicate(x5);

// 3. Same Frequency

function sameFrequency(num1, num2) {
    // step 1: convert number to string
    const str1 = num1.toString();
    const str2 = num2.toString();

    // step 2: check the length
    if (str1.length !== str2.length) {
        return false;
    }

    // ste 3: build the charmap for both strings
    const obj1 = buildCharMap(str1);
    const obj2 = buildCharMap(str2);

    // step 4: loop through any one of object and compare with other
    for (let key in obj1) {
        const value1 = obj1[key];
        const value2 = obj2[key];
        if (value1 !== value2) {
            return false;
        }
    }

    return true;
}

console.log(sameFrequency(1821, 2811)); //true

console.log(sameFrequency(22, 222)); //false

console.log(sameFrequency(34, 14)); //false

console.log(sameFrequency(3589578, 5879385)); //true


// 4. Anagrams

function isAnagram(str1, str2) {
    // step 1: check the length
    if (str1.length !== str2.length) {
        return false;
    }

    // ste 2: build the charmap for both strings
    const obj1 = buildCharMap(str1);
    const obj2 = buildCharMap(str2);

    // step 3: loop through any one of object and compare with other
    for (let key in obj1) {
        const value1 = obj1[key];
        const value2 = obj2[key];
        if (value1 !== value2) {
            return false;
        }
    }

    return true;
}

console.log(isAnagram('aaz', 'zza')); //false

console.log(isAnagram('rat', 'car')); //false

console.log(isAnagram('hello', 'olleh')); //true

5. Compare arrays
It accepts 2 arrays. If every value of first array has its corresponding value squared in the second array.
The frequency must also be same. Order does not matter.


function compareArrays(arr1, arr2) {
    //Step 1
    if (arr1.length !== arr2.length) {
        return false;
    }

    // step 2: build the character map
    const obj1 = buildCharMap(arr1);
    const obj2 = buildCharMap(arr2);

    // step 3: looping
    for (let key in obj1) {
        // check if key square is present in the obj2
        if (!obj2[key * key]) {
            return false;
        }
        // check if obj1[key] is same as obj2[key square]
        if (obj2[key * key] !== obj1[key]) {
            return false;
        }
    }

    return true;
}

console.log(compareArrays([1, 2, 3], [4, 1, 9])); // true

console.log(compareArrays([1, 2, 3], [1, 9])); // false

console.log(compareArrays([1, 2, 1], [4, 4, 1])); // false

6. Two Sum

Sum of 2 numbers should be equal to the target

[2, 4, 5, 6, 8], target: 6

Return:
[0, 1]

Target: 14
[3, 4]

Target: 100
null, []

// O (n)
/* obj = {
    2: 0,
    4: 1,
    5: 2,
    6: 3,
    8: 4
};*/
function twoSum(input, target) {
    const obj = {};
    for (var i = 0; i < input.length; i++) {
        var val = input[i];
        console.log('val is ', val);
        var check = target - val;
        console.log('check is ', check);
        console.log('obj is ', obj);

        if (obj[check] || obj[check] === 0) {
            return [obj[check], i];
        } else {
            obj[val] = i;
        }
        // i = 0;, obj = {2: 0}
        // i = 1, obj = {2: 0, 4: 1}, check: 4
        // i = 2, obj = {2: 0, 4: 1, 5: 2}, check: 3
        // i = 3, obj = {2: 0, 4: 1, 5: 2, 6: 3} check: 2
        // i = 4, obj = {2: 0, 4: 1, 5: 2, 6: 3, 8: 4}
    }
    return null;
}
twoSum([2, 4, 5, 6, 8], 6) //0 and 1

twoSum([2, 4, 5, 6, 8], 7); //0 and 2

twoSum([2, 4, 5, 6, 8], 8); //0 and 3
/*
maxChars('helloHHH')
return: character l.

maxChars('jackandjill')
return j or l, both are fine.

maxChars() // return null;
*/

// create a object for each character
/*
    obj = {
        h: 1,
        e: 1,
        l: 3,
        o: 2,
        w: 1,
        r: 1,
        d: 1
    }
*/

function maxChars(str) {
  if (!str) return null;
  const newStr = str.toLowerCase();
  // 2 variables which will keep track of records
  let max = 0;
  let maxC = '';
  const obj = {};
  // loop through each charachter of the string
  for (let i of newStr) {
    if (obj[i]) {
      obj[i]++;
    } else {
      obj[i] = 1;
    }

    if (obj[i] > max) {
      max = obj[i];
      maxC = i;
    }
  }
  console.log('obj is ', obj, ', max is ', max, ', max char is ', maxC);
  return maxC;
}

const x = maxChars('aaabbbbbccc');
console.log('x is ', x); // l

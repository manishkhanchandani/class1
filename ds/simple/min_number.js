/*
Find the minimum number in an array
minNumber([4, 54, 33, 100, 22, 122])
Return will be 122
*/

function minNumber(arr) {
  // create a temporary min variable
  let min = arr[0];
  // loop through the array
  for (let i of arr) {
    console.log('element is ', i, ', min is ', min);
    // we will check if current element in array is less than min,
    if (i < min) {
      // if it is less than min, we assign new value to min
      min = i;
    }
  }

  return min;
}

var num;
num = minNumber([4, 54, 33, 100, 22, 122]);
console.log('num is ', num);

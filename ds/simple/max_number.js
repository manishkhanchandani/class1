/*
Find the maximum number in an array
maxNumber([4, 54, 33, 100, 22, 122])
Return will be 122
*/

function maxNumber(arr) {
  // create a temporary max variable
  let max = arr[0];
  // loop through the array
  for (let i of arr) {
    console.log('element is ', i, ', max is ', max);
    // we will check if current element in array is greater than max,
    if (i > max) {
      // if it is greater than max, we assign new value to max
      max = i;
    }
  }

  return max;
}

var num;
num = maxNumber([4, 54, 33, 100, 22, 122]);
console.log('num is ', num);
